package com.luxoft.dnepr.courses.unit3.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

import static com.luxoft.dnepr.courses.unit3.controller.Deck.costOf;

public class GameController {
    private static GameController controller;
    private static final int BLACKJACK = 21;
    private static final int DEFAULT_NUMBER_OF_DECK = 1;
    private List<Card> deck;
    private List<Card> playersHand;
    private List<Card> dealersHand;

    private GameController() {
        deck = Deck.createDeck(DEFAULT_NUMBER_OF_DECK);
        playersHand = new ArrayList<>();
        dealersHand = new ArrayList<>();
    }

    public static GameController getInstance() {
        if (controller == null) {
            controller = new GameController();
        }

        return controller;
    }

    public void newGame() {
        deck = Deck.createDeck(DEFAULT_NUMBER_OF_DECK);
        playersHand.clear();
        dealersHand.clear();
        newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                Collections.shuffle(deck);
            }
        });
    }

    /**
     * Создает новую игру.
     * - перемешивает колоду (используйте для этого shuffler.shuffle(list))
     * - раздает две карты игроку
     * - раздает одну карту диллеру.
     *                                                                    в
     * @param shuffler
     */
    void newGame(Shuffler shuffler) {

        shuffler.shuffle(deck);
        for (int i = 0; i < 3; i++) {
            if (i < 2) {
                playersHand.add(deck.remove(0));
            } else {
                dealersHand.add(deck.remove(0));
            }
        }
    }

    /**
     * Метод вызывается когда игрок запрашивает новую карту.
     * - если сумма очков на руках у игрока больше максимума или колода пуста - ничего не делаем
     * - если сумма очков меньше - раздаем игроку одну карту из коллоды.
     *
     * @return true если сумма очков у игрока меньше максимума (или равна) после всех операций и false если больше.
     */
    public boolean requestMore() {
        int playersHandCost = costOf(playersHand);

        if (playersHandCost > BLACKJACK || deck.size() == 0) {
            return false;
        }
        playersHand.add(deck.remove(0));
        return true;

    }

    /**
     * Вызывается когда игрок получил все карты и хочет чтобы играло казино (диллер).
     * Сдаем диллеру карты пока у диллера не наберется 17 очков.
     */
    public void requestStop() {
        int dealersHandCost = costOf(dealersHand);

        while (dealersHandCost < 17) {
            Card tempCard = deck.remove(0);
            dealersHand.add(tempCard);
            dealersHandCost += tempCard.getCost();
        }
    }

    /**
     * Сравниваем руку диллера и руку игрока.
     * Если у игрока больше максимума - возвращаем WinState.LOOSE (игрок проиграл)
     * Если у игрока меньше чем у диллера и у диллера не перебор - возвращаем WinState.LOOSE (игрок проиграл)
     * Если очки равны - это пуш (WinState.PUSH)
     * Если у игрока больше чем у диллера и не перебор - это WinState.WIN (игрок выиграл).
     */
    public WinState getWinState() {

        int playersHandCost = costOf(playersHand);
        int dealersHandCost = costOf(dealersHand);

        if (playersHandCost > BLACKJACK) {
            return WinState.LOOSE;
        }

        if (playersHandCost < dealersHandCost && dealersHandCost <= BLACKJACK) {
            return WinState.LOOSE;
        } else if (playersHandCost == dealersHandCost) {
            return WinState.PUSH;
        }
        return WinState.WIN;

    }

    /**
     * Возвращаем руку игрока
     */
    public List<Card> getMyHand() {
        return playersHand;
    }

    /**
     * Возвращаем руку диллера
     */
    public List<Card> getDealersHand() {
        return dealersHand;
    }

    public List<Card> getDeck() {
        return deck;
    }
}
