package com.luxoft.dnepr.courses.unit3.controller;

import java.util.ArrayList;
import java.util.List;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;

public class Deck {

    private Deck() {

    }

    public static List<Card> createDeck(int size) {

        if (size < 1) {
            size = 1;
        } else if (size > 10) {
            size = 10;
        }

        ArrayList<Card> listOfCard = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            for (Suit suit : Suit.values()) {
                for (Rank rank : Rank.values()) {
                    listOfCard.add(new Card(rank, suit));
                }
            }
        }

        return listOfCard;
    }

    public static int costOf(List<Card> hand) {
        int summaryCost = 0;
        for (Card card : hand) {
            summaryCost += card.getCost();
        }
        return summaryCost;
    }
}
