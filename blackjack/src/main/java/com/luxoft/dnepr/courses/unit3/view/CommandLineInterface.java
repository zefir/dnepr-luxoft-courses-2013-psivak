package com.luxoft.dnepr.courses.unit3.view;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

import static com.luxoft.dnepr.courses.unit3.view.Command.*;

public class CommandLineInterface {

    public static String WIN_MESSAGE = "Congrats! You win!";
    public static String LOOSE_MESSAGE = "Sorry, today is not your day. You loose.";
    public static String PUSH_MESSAGE = "Push. Everybody has equal amount of points.";
    public static String HELP_MESSAGE = "Usage: \n" +
            "\thelp - prints this message\n" +
            "\thit - requests one more card\n" +
            "\tstand - I'm done - lets finish\n" +
            "\texit - exits game";

    private Scanner scanner;
    private PrintStream output;

    public CommandLineInterface(PrintStream output, InputStream input) {
        this.scanner = new Scanner(input);
        this.output = output;
    }

    public void play() {
        output.println("Console Blackjack application.\n" +
                "Author: PSivak\n" +
                "(C) Luxoft 2013\n");

        GameController controller = GameController.getInstance();
        controller.newGame();
        output.println();
        printState(controller);
        while (scanner.hasNext()) {
            String command = scanner.next();
            if (!execute(command, controller)) {
                return;
            }
        }
    }

    /**
     * Выполняем команду, переданную с консоли. Список разрешенных комманд можно найти в классе {@link Command}.
     * Используйте методы контроллера чтобы обращаться к логике игры. Этот класс должен содержать только интерфейс.
     * Если этот метод вернет false - игра завершится.
     * <p/>
     * Более детальное описание формата печати можно узнать посмотрев код юниттестов.
     * <p/>
     *
     * @see com.luxoft.dnepr.courses.unit3.view
     *      <p/>
     *      Описание команд:
     *      Command.HELP - печатает помощь.
     *      Command.MORE - еще одну карту и напечатать Состояние (GameController.requestMore())
     *      если после карты игрок проиграл - напечатать финальное сообщение и выйти
     *      Command.STOP - игрок закончил, теперь играет диллер (GameController.requestStop())
     *      после того как диллер сыграл напечатать:
     *      Dealer turn:
     *      пустая строка
     *      состояние
     *      пустая строка
     *      финальное сообщение
     *      Command.EXIT - выйти из игры
     *      <p/>
     *      Состояние:
     *      рука игрока (total вес)
     *      рука диллера (total вес)
     *      <p/>
     *      например:
     *      3 J 8 (total 21)
     *      A (total 11)
     *      <p/>
     *      Финальное сообщение:
     *      В зависимости от состояния печатаем:
     *      Congrats! You win!
     *      Push. Everybody has equal amount of points.
     *      Sorry, today is not your day. You loose.
     *      <p/>
     *      Постарайтесь уделить внимание чистоте кода и разделите этот метод на несколько подметодов.
     */

    private boolean execute(String command, GameController controller) {
        switch (command) {
            case EXIT:
                return false;
            case HELP:
                output.println(HELP_MESSAGE);
                return true;
            case MORE:
                boolean isMore = controller.requestMore();
                printState(controller);
                if (!isMore) {
                    if (controller.getWinState() == WinState.LOOSE) {
                        output.println(LOOSE_MESSAGE);
                    }
                    return false;
                }
                return true;
            case STOP:
                output.println("Dealer turn:\n");
                controller.requestStop();
                printState(controller);
                output.println();
                printResultMessage(controller);
                return false;
            default:
                output.println("Invalid command");
                return true;
        }
    }

    private void printResultMessage(GameController controller) {
        if (controller.getWinState() == WinState.LOOSE) {
            output.println(LOOSE_MESSAGE);
        }
        if (controller.getWinState() == WinState.WIN) {
            output.println(WIN_MESSAGE);
        }
        if (controller.getWinState() == WinState.PUSH) {
            output.println(PUSH_MESSAGE);
        }
    }

    private void printState(GameController controller) {
        List<Card> myHand = controller.getMyHand();

        format(myHand);

        List<Card> dealersHand = controller.getDealersHand();

        format(dealersHand);
    }

    private void format(List<Card> myHand) {
        int handsCount = 0;
        for (Card card : myHand) {
            handsCount += card.getCost();
            output.print(card.getRank().getName() + " ");
        }
        output.println("(total " + handsCount + ")");
    }
}
