var requestMore = "service?method=requestMore";
var stop = "service?method=stop";
var winState = "service?method=winState";
var newGame = "service?method=newGame";

$(document).ready(function(){
    $("#btnNewGame").click(function() {
        prepareGame();
        processJSON(newGame, "BOTH");

    });
});

function btnHit() {
    $("#phand div").remove();
    processJSON(requestMore, "PHAND");

    $.getJSON(winState, function(json) {
        if (json.result === "LOOSE") {
            $(".m1").html("You LOOSE!");
            gameFinished();
        }
    });
}
function btnStop() {
    $("#dhand div").remove();
    processJSON(stop, "DHAND");
    getWinState();
}
function processJSON(method, hand) {

    $.getJSON(method, function(json) {
        if (hand === "BOTH" || hand === "PHAND") {
            $.each(json.myhand, function() {
                $d = $("<div>");
                $d.addClass("current_phand").appendTo("#phand");
                $("<img>").appendTo($d).attr('src', 'img/cards/' + this['suit'].toLowerCase() + '/' + this['rank'] + '.jpg').fadeOut('slow').fadeIn('slow');
            });
        }
        if (hand === "BOTH" || hand === "DHAND") {
            $.each(json.dealershand, function() {
                $d = $("<div>");
                $d.addClass("current_dhand").appendTo("#dhand");
                $("<img>").appendTo($d).attr('src', 'img/cards/' + this['suit'].toLowerCase() + '/' + this['rank'] + '.jpg').fadeOut('slow').fadeIn('slow');
            });
        }
    });
}
function getWinState() {
    $.getJSON("service?method=winState", function(json) {
        if (json.result === "LOOSE") {
            $(".m1").html("You LOOSE!");
        } else {
            $(".m1").html("You WIN!");
        }
    });
    gameFinished();
}
function gameFinished() {
    $(".message").removeClass("meshid");
    $("#btnHit").unbind('click');
    $("#btnStop").unbind('click');
}
function prepareGame() {
    $("#btnHit").click(btnHit);
    $("#btnStop").click(btnStop);
    $("#dhand div").remove();
    $("#phand div").remove();
    $("div.current_dhand").remove();
    $("div.current_phand").remove();
    $(".m1").html('');
    $(".message").addClass("meshid");
}