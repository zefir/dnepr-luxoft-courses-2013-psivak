package com.luxoft.dnepr.courses.unit3.controller;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;

import static org.junit.Assert.*;

public class DeckTest {

    @Test
    public void testCreate() {
        assertEquals(52, Deck.createDeck(1).size());
        assertEquals(104, Deck.createDeck(2).size());
        assertEquals(208, Deck.createDeck(4).size());
        assertEquals(52 * 10, Deck.createDeck(10).size());

        assertEquals(52 * 10, Deck.createDeck(11).size());
        assertEquals(52, Deck.createDeck(-1).size());

        List<Card> deck = Deck.createDeck(2);
        int i = 0;

        for (int deckN = 0; deckN < 2; deckN++) {
            for (Suit suit : Suit.values()) {
                for (Rank rank : Rank.values()) {
                    assertEquals(suit, deck.get(i).getSuit());
                    assertEquals(rank, deck.get(i).getRank());
                    assertEquals(rank.getCost(), deck.get(i).getCost());
                    i++;
                }
            }
        }
    }

    @Test
    public void testCostOf() {

        assertEquals(380, Deck.costOf(Deck.createDeck(1)));
        assertEquals(1900, Deck.costOf(Deck.createDeck(5)));
        assertEquals(380, Deck.costOf(Deck.createDeck(-1)));
        assertEquals(3800, Deck.costOf(Deck.createDeck(50)));
        assertEquals(23, Deck.costOf(Arrays.<Card>asList(
                new Card[]{
                        new Card(Rank.RANK_2, Suit.CLUBS),
                        new Card(Rank.RANK_10, Suit.CLUBS),
                        new Card(Rank.RANK_ACE, Suit.CLUBS)})));

    }
}
