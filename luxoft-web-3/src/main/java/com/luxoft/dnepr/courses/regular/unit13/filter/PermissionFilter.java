package com.luxoft.dnepr.courses.regular.unit13.filter;

import com.luxoft.dnepr.courses.regular.unit13.entity.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class PermissionFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //NOP
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute("user");

        if (user != null) {
            if (request.getRequestURI().contains("admin") && !user.getRole().equals("admin")) {
                response.sendRedirect(request.getContextPath() + "/user");
            }

        } else {
            response.sendRedirect(request.getContextPath());
            return;
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        //NOP
    }
}
