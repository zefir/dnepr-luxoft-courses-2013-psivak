package com.luxoft.dnepr.courses.regular.unit13.listener;

import com.luxoft.dnepr.courses.regular.unit13.entity.User;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.concurrent.atomic.AtomicInteger;

public class CountActiveUserListener implements HttpSessionAttributeListener {
    @Override
    public void attributeAdded(HttpSessionBindingEvent httpSessionBindingEvent) {
        HttpSession session = httpSessionBindingEvent.getSession();
        ServletContext ctx = session.getServletContext();
        User user = (User) session.getAttribute("user");
        if (user != null) {
            if (user.getRole().equals("admin")) {
                ((AtomicInteger)ctx.getAttribute("adminSessions")).incrementAndGet();
            } else {
                ((AtomicInteger)ctx.getAttribute("userSessions")).incrementAndGet();
            }
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent httpSessionBindingEvent) {
        HttpSession session = httpSessionBindingEvent.getSession();
        ServletContext ctx = session.getServletContext();
        User user = (User) httpSessionBindingEvent.getValue();
        if (user != null) {
            if (user.getRole().equals("admin")) {
                ((AtomicInteger)ctx.getAttribute("adminSessions")).decrementAndGet();
            } else {
                ((AtomicInteger)ctx.getAttribute("userSessions")).decrementAndGet();
            }
        }
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent httpSessionBindingEvent) {
        //NOP
    }
}
