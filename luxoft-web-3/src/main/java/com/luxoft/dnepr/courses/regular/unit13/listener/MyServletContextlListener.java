package com.luxoft.dnepr.courses.regular.unit13.listener;

import com.luxoft.dnepr.courses.regular.unit13.entity.User;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class MyServletContextlListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext ctx = servletContextEvent.getServletContext();
        Map<String, User> userDB = new HashMap<>();
        InputStream is = ctx.getResourceAsStream("META-INF/" + ctx.getInitParameter("users"));

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document doc = null;
        DocumentBuilder builder = null;

        try {
            builder = factory.newDocumentBuilder();
            doc = builder.parse(is);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new RuntimeException("Error XML Parsing", e);
        }

        Element root = doc.getDocumentElement();
        NodeList children = root.getChildNodes();
        root.getElementsByTagName("user");
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof Element) {
                userDB.put(((Element) child).getAttribute("name"),
                        new User(((Element) child).getAttribute("name"),
                           ((Element) child).getAttribute("role"),
                           ((Element) child).getAttribute("password")));
            }
        }
        ctx.setAttribute("userDB", userDB);
        AtomicInteger userSessions = new AtomicInteger(0);
        AtomicInteger adminSessions = new AtomicInteger(0);
        AtomicInteger httpRequests = new AtomicInteger(0);
        ctx.setAttribute("userSessions", userSessions);
        ctx.setAttribute("adminSessions", adminSessions);
        ctx.setAttribute("httpRequests", httpRequests);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ServletContext ctx = servletContextEvent.getServletContext();
        ctx.removeAttribute("userDB");
        ctx.removeAttribute("userSessions");
        ctx.removeAttribute("adminSessions");
        ctx.removeAttribute("httpRequests");
    }
}
