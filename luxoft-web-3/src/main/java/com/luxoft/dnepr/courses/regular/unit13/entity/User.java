package com.luxoft.dnepr.courses.regular.unit13.entity;

public class User {
    private String login;
    private String role;
    private String password;

    public User() {

    }

    public User(String login, String role, String password) {
        this.login = login;
        this.role = role;
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
