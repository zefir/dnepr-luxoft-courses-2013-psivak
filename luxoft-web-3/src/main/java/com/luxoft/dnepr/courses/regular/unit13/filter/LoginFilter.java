package com.luxoft.dnepr.courses.regular.unit13.filter;

import com.luxoft.dnepr.courses.regular.unit13.entity.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

public class LoginFilter implements Filter {

    public static String BASE_PAGE = "";
    public static String ADMIN_PAGE = "admin/sessionData";
    public static String USER_PAGE = "user";

    private ServletContext ctx;
    private Map<String, User> userDB;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ctx = filterConfig.getServletContext();
        userDB = (Map<String, User>) ctx.getAttribute("userDB");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        HttpSession session = request.getSession(false);

        if (session.getAttribute("user") == null) {
            if (userDB.containsKey(login) && userDB.get(login).getPassword().equals(password)) {
                session = request.getSession();

                session.setAttribute("user", userDB.get(login));
            }
        }

        if (session.getAttribute("user") == null) {
            request.setAttribute("auth", 0);
            request.getRequestDispatcher(BASE_PAGE).forward(request, response);
            return;
        }

        User user = (User) session.getAttribute("user");
        if (user.getRole().equals("admin")) {
            request.getRequestDispatcher(ADMIN_PAGE).forward(request, response);
        } else if (user.getRole().equals("user")) {
            request.getRequestDispatcher(USER_PAGE).forward(request, response);
        } else {
            request.getRequestDispatcher(BASE_PAGE).forward(request, response);
            return;
        }

    }

    @Override
    public void destroy() {
        ctx = null;
        userDB = null;
    }
}
