package com.luxoft.dnepr.courses.regular.unit13.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import java.util.concurrent.atomic.AtomicInteger;

public class CountHttpRequestListener implements ServletRequestListener {
    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
        //NOP
    }

    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent) {
        ServletContext ctx = servletRequestEvent.getServletContext();
        ((AtomicInteger)ctx.getAttribute("httpRequests")).incrementAndGet();
    }
}
