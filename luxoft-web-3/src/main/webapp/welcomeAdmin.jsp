<%@ page import="com.luxoft.dnepr.courses.regular.unit13.entity.User" %>
<%@ page import="java.util.concurrent.atomic.AtomicInteger" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>WelcomeAdminPage</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<style>
    .well {
        position: absolute;
        top: 0;
        right: 0;
    }
    .table {
        margin-top: 200px;
        margin-left: 400px;
    }

</style>
<body>
<div class="well well-lg"><a href="logout"><strong>Logout</strong></a></div>
<div class="container col-md-3">
<table class="table table-bordered">
    <tr class="success">
        <th class="col-md-2">Parameter</th>
        <th class="col-md-1">Value</th>
    </tr>
    <tr>
        <td class="col-md-2">Active Sessions</td>
        <td class="col-md-1"><%= ((AtomicInteger)application.getAttribute("userSessions")).get() + ((AtomicInteger)application.getAttribute("adminSessions")).get() %></td>
    </tr>
    <tr>
        <td class="col-md-2">Active Sessions (ROLE user)</td>
        <td class="col-md-1"><%= ((AtomicInteger)application.getAttribute("userSessions")).get() %></td>
    </tr>
    <tr>
        <td class="col-md-2">Active Sessions (ROLE admin)</td>
        <td class="col-md-1"><%= ((AtomicInteger)application.getAttribute("adminSessions")).get() %></td>
    </tr>
    <tr>
        <td class="col-md-2">Total Count of HttpRequests</td>
        <td class="col-md-1"><%= ((AtomicInteger)application.getAttribute("httpRequests")).get() %></td>
    </tr>
</table>
</div>
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>