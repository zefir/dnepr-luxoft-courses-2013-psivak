<%@ page import="com.luxoft.dnepr.courses.regular.unit13.entity.User" %>
<html>
<head>
    <title>WelcomeUserPage</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<style>
    .well{
        position: absolute;
        top: 0;
        right: 0;
    }
</style>
<body>
    <div class="well well-lg"><a href="logout"><strong>Logout</strong></a></div>
    <h1 class="text-center text-success">
    <%
        User user = (User) session.getAttribute("user");
        String login = user.getLogin();
    %>
    <%= "Hello, " + login + "!" %>
    </h1>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>