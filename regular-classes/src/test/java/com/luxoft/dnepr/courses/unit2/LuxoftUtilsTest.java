package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Circle;
import com.luxoft.dnepr.courses.unit2.model.Figure;
import com.luxoft.dnepr.courses.unit2.model.Hexagon;
import com.luxoft.dnepr.courses.unit2.model.Square;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class LuxoftUtilsTest {

    public static final String[] expectedArrayTrue = {"A", "Z", "a", "b", "c", "d", "e"};
    public static final String[] expectedArrayFalse = {"e", "d", "c", "b", "a", "Z", "A"};
    Square square = new Square(2);
    Circle circle = new Circle(2);
    Hexagon hexagon = new Hexagon(2);
    List<Figure> list = new ArrayList<>();

    @Before
    public void setUp() {
        list.add(square);
        list.add(circle);
        list.add(hexagon);
    }

    @Test
    public void testSortArray() throws Exception {

        Assert.assertArrayEquals(expectedArrayTrue, LuxoftUtils.sortArray(new String[]{"b", "d", "c", "a", "e", "Z", "A"}, true));
        Assert.assertArrayEquals(expectedArrayFalse, LuxoftUtils.sortArray(new String[]{"b", "d", "c", "a", "e", "Z", "A"}, false));
        Assert.assertArrayEquals(new String[0], LuxoftUtils.sortArray(new String[0], true));
        Assert.assertArrayEquals(new String[]{"A"}, LuxoftUtils.sortArray(new String[]{"A"}, true));

    }

    @Test(expected = IllegalArgumentException.class)
    public void testBorderSortArray() {
        LuxoftUtils.sortArray(null, true);
    }

    @Test
    public void testWordAverageLength() {
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength("    I     have    a cat    "), 0.000001);
        Assert.assertEquals(0, LuxoftUtils.wordAverageLength(""), 0.000001);
        Assert.assertEquals(6, LuxoftUtils.wordAverageLength("   asdasd   "), 0.000001);
        Assert.assertEquals(7, LuxoftUtils.wordAverageLength("Mamamia"), 0.000001);
        Assert.assertEquals(0, LuxoftUtils.wordAverageLength("     "), 0.000001);
    }

    @Test
    public void testReverseWords() {
        Assert.assertEquals("cba trf rgrg ", LuxoftUtils.reverseWords("abc frt grgr "));
        Assert.assertEquals(" янукович   покращено ", LuxoftUtils.reverseWords(" чивокуня   онещаркоп "));
        Assert.assertEquals(" янукович  ", LuxoftUtils.reverseWords(" чивокуня  "));
        Assert.assertEquals("", LuxoftUtils.reverseWords(""));
        Assert.assertEquals("  ", LuxoftUtils.reverseWords("  "));
    }

    @Test
    public void testGetCharEntries() {
        Assert.assertArrayEquals(new char[]{'a', 'I', 'c', 'e', 'h', 't', 'v'}, LuxoftUtils.getCharEntries("I have a cat"));
        Assert.assertArrayEquals(new char[]{'A', 'S', 'a', 'z'}, LuxoftUtils.getCharEntries("aaaAAA zzzSSS"));
    }

    @Test
    public void testCalculateOverallArea() {
        Assert.assertEquals(26.958675459772437, LuxoftUtils.calculateOverallArea(list), 0.000001);
    }
}
