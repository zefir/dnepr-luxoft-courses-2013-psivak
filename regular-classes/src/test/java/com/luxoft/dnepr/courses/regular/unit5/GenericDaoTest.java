package com.luxoft.dnepr.courses.regular.unit5;

import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.dao.GenericDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage.getEntities;
import static org.junit.Assert.*;

public class GenericDaoTest {

    GenericDao<Redis> redisGenericDao = new RedisDaoImpl();
    GenericDao<Employee> employeeIDao = new EmployeeDaoImpl();

    @Before
    public void init() {
        getEntities().clear();
        getEntities().put(1L, new Employee(1L));
        getEntities().put(2L, new Employee(3L));
        getEntities().put(3L, new Employee(2L));
        getEntities().put(4L, new Redis(4L));
        getEntities().put(5L, new Redis(5L));
        getEntities().put(6L, new Redis(6L));

    }

    @Test
    public void getTest() {

        assertEquals(Long.valueOf(1), employeeIDao.get(1L).getId());
        assertNull(employeeIDao.get(6L));
    }

    @Test
    public void saveTest1() throws UserAlreadyExist {

        Redis redis = redisGenericDao.create();
        assertEquals(Long.valueOf(7), redisGenericDao.save(redis).getId());
        assertEquals(Long.valueOf(10), redisGenericDao.save(new Redis(10L)).getId());
        assertEquals(8, EntityStorage.getEntities().size());
    }

    @Test(expected = UserAlreadyExist.class)
    public void saveTest2() throws UserAlreadyExist {
        redisGenericDao.save(new Redis(6L)).getId();
    }

    @Test
    public void deleteTest() {

        assertFalse(redisGenericDao.delete(0));
        assertTrue(redisGenericDao.delete(1));
        assertEquals(5, EntityStorage.getEntities().size());

    }

    @Test(expected = UserNotFound.class)
    public void updateTest1() throws UserNotFound {
        Redis redis = redisGenericDao.create();
        redisGenericDao.update(redis);
    }

    @Test
    public void updateTest2() throws UserNotFound {
        Redis redis = redisGenericDao.create();
        redis.setWeight(10);
        redis.setId(6L);
        Assert.assertEquals(10, redisGenericDao.update(redis).getWeight());
        assertEquals(6, EntityStorage.getEntities().size());
    }
}
