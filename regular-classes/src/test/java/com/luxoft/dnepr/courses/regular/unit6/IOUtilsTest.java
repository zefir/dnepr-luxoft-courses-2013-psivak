package com.luxoft.dnepr.courses.regular.unit6;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class IOUtilsTest {

    Person person = new PersonImpl();
    Person father = new PersonImpl();
    Person mother = new PersonImpl();
    Person grandFather = new PersonImpl();
    Person grandMother = new PersonImpl();
    FamilyTree familyTree = FamilyTreeImpl.create(person);

    @Before
    public void setUp() {

        person.setAge(20);
        person.setEthnicity("ukrainian");
        person.setGender(Gender.FEMALE);
        person.setName("Galja");

        father.setAge(40);
        father.setEthnicity("ukrainian");
        father.setGender(Gender.MALE);
        father.setName("Vasiliy");
        person.setFather(father);

        mother.setAge(39);
        mother.setEthnicity("kenian");
        mother.setGender(Gender.FEMALE);
        mother.setName("Nairobi");
        person.setMother(mother);

        grandFather.setAge(60);
        grandFather.setEthnicity("kenian");
        grandFather.setGender(Gender.MALE);
        grandFather.setName("Okoruka");
        mother.setFather(grandFather);

        grandMother.setAge(59);
        grandMother.setEthnicity("kenian");
        grandMother.setGender(Gender.FEMALE);
        grandMother.setName("Chaka");
        mother.setMother(grandMother);
    }

    @Test
    public void saveTest() throws IOException {
        IOUtils.save("test.txt", familyTree);
    }

    @Test
    public void loadTest() throws IOException, ClassNotFoundException {
        FamilyTree ft = IOUtils.load("test.txt");
        System.out.println(ft.getRoot());
        assertEquals("Galja", ft.getRoot().getName());
        assertEquals("Vasiliy", ft.getRoot().getFather().getName());
        assertEquals("Nairobi", ft.getRoot().getMother().getName());
        assertEquals("Chaka", ft.getRoot().getMother().getMother().getName());
    }
}
