package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.*;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book)book.clone();

        assertFalse(book.getPublicationDate() == cloned.getPublicationDate());
        assertTrue(book.equals(cloned));
        assertTrue(book.getName().equals(cloned.getName()));
        assertTrue(book.getCode().equals(cloned.getCode()));
        assertTrue(book.getPublicationDate().equals(cloned.getPublicationDate()));
        assertTrue(book.code == cloned.code);
    }

    @Test
    public void testEquals() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        assertFalse(book.equals(null));
        assertTrue(book.equals(new Book("code", "Thinking in Java", 20, new GregorianCalendar(2006, 0, 1).getTime())));
        assertFalse(book.equals(new Book("code1", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime())));
        assertFalse(book.equals(new Book("code1", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 2).getTime())));
        assertFalse(book.equals(new Book("code1", "Thiking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime())));
    }
}
