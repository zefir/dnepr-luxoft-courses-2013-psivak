package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static junit.framework.Assert.assertEquals;

public class WalletTest {

    WalletInterface wallet = new Wallet();

    @Before
    public void setUp() {
        wallet.setAmount(BigDecimal.valueOf(100));
        wallet.setId(Long.valueOf(1));
        wallet.setMaxAmount(BigDecimal.valueOf(1000));
        wallet.setStatus(WalletStatus.BLOCKED);
    }

    @Test(expected = WalletIsBlockedException.class)
    public void testCheckWithdrawalIfBlocked() throws WalletIsBlockedException, InsufficientWalletAmountException {
        wallet.checkWithdrawal(BigDecimal.valueOf(10));
    }

    @Test(expected = InsufficientWalletAmountException.class)
    public void testCheckWithdrawalIfInsufficient() throws WalletIsBlockedException, InsufficientWalletAmountException {
        wallet.setStatus(WalletStatus.ACTIVE);
        wallet.checkWithdrawal(BigDecimal.valueOf(150));
    }

    @Test(expected = WalletIsBlockedException.class)
    public void testCheckTransferIfBlocked() throws WalletIsBlockedException, LimitExceededException {
        wallet.checkTransfer(BigDecimal.valueOf(10));
    }

    @Test(expected = LimitExceededException.class)
    public void testCheckTransferIfExceeded() throws WalletIsBlockedException, LimitExceededException {
        wallet.setStatus(WalletStatus.ACTIVE);
        wallet.checkTransfer(BigDecimal.valueOf(1000));
    }

    @Test
    public void testWithdraw() throws WalletIsBlockedException, InsufficientWalletAmountException {
        wallet.setStatus(WalletStatus.ACTIVE);
        wallet.withdraw(BigDecimal.valueOf(50));
        assertEquals(BigDecimal.valueOf(50), wallet.getAmount());
    }

    @Test
    public void testTransfer() {
        wallet.setStatus(WalletStatus.ACTIVE);
        assertEquals(BigDecimal.valueOf(100), wallet.getAmount());
        wallet.transfer(BigDecimal.valueOf(900));
        assertEquals(BigDecimal.valueOf(1000), wallet.getAmount());
    }
}
