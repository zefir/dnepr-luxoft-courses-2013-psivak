package com.luxoft.dnepr.courses.unit1;

import org.junit.Assert;
import org.junit.Test;

public class LuxoftUtilsTest {

    @Test
    public void getMonthNameTest() {

        Assert.assertEquals("Январь", LuxoftUtils.getMonthName(1, "ru"));
        Assert.assertEquals("December", LuxoftUtils.getMonthName(12, "en"));

    }

    @Test
    public void getMonthNameBorderTest() {

        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(1, "blatnoy"));
        Assert.assertEquals("Неизвестный месяц", LuxoftUtils.getMonthName(13, "ru"));
        Assert.assertEquals("Unknown Month", LuxoftUtils.getMonthName(0, "en"));

    }

    @Test
    public void binaryToDecimalTest() {

        Assert.assertEquals("10", LuxoftUtils.binaryToDecimal("1010"));
        Assert.assertEquals("127", LuxoftUtils.binaryToDecimal("1111111"));

    }

    @Test
    public void binaryToDecimalBorderTest() {

        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("w111"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("-127"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal(null));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal(""));

    }

    @Test
    public void  decimalToBinaryTest() {

        Assert.assertEquals("1010", LuxoftUtils.decimalToBinary("10"));
        Assert.assertEquals("1111111", LuxoftUtils.decimalToBinary("127"));
        Assert.assertEquals("1", LuxoftUtils.decimalToBinary("1"));

    }

    @Test
    public void  decimalToBinaryBorderTest() {

        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("w121"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("-127"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary(""));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary(null));

    }

    @Test
    public void sortArrayTest() {

        Assert.assertArrayEquals(new int[]{1, 2, 3, 4, 5}, LuxoftUtils.sortArray(new int[]{5, 4, 3, 2, 1}, true));
        Assert.assertArrayEquals(new int[]{5, 4, 3, 2, 1}, LuxoftUtils.sortArray(new int[]{1, 2, 3, 4, 5}, false));

    }

    @Test
    public void sortArrayBorderTest() {

        Assert.assertArrayEquals(null, LuxoftUtils.sortArray(null, true));

    }

}
