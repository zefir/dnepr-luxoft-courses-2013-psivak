package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class BankTest {

    Bank bank = new Bank("1.7");

    @Before
    public void setUp() {
        WalletInterface wallet = new Wallet();
        wallet.setAmount(BigDecimal.valueOf(100));
        wallet.setMaxAmount(BigDecimal.valueOf(1001));
        wallet.setStatus(WalletStatus.ACTIVE);
        wallet.setId(Long.valueOf(1));
        UserInterface user = new User();
        user.setId(Long.valueOf(1));
        user.setName("John");
        user.setWallet(wallet);
        Map<Long, UserInterface> users = new HashMap<>();
        users.put(user.getId(), user);
        wallet = new Wallet();
        wallet.setAmount(BigDecimal.valueOf(1000));
        wallet.setMaxAmount(BigDecimal.valueOf(1002));
        wallet.setStatus(WalletStatus.ACTIVE);
        wallet.setId(Long.valueOf(2));
        user = new User();
        user.setId(Long.valueOf(2));
        user.setName("Peter");
        user.setWallet(wallet);
        users.put(user.getId(), user);
        bank.setUsers(users);
    }

    @Test(expected = IllegalJavaVersionError.class)
    public void testBankConstructor() {
        Bank bank = new Bank("1.6");
    }

    @Test(expected = NoUserFoundException.class)
    public void testMakeMoneyTransactionIfNoUser() throws TransactionException, NoUserFoundException {
        bank.makeMoneyTransaction(Long.valueOf(10), Long.valueOf(2),BigDecimal.TEN);
    }

    @Test(expected = TransactionException.class)
    public void testMakeMoneyTransactionIfBlocked() throws TransactionException, NoUserFoundException {
        bank.getUsers().get(Long.valueOf(1)).getWallet().setStatus(WalletStatus.BLOCKED);
        bank.makeMoneyTransaction(Long.valueOf(1), Long.valueOf(2),BigDecimal.TEN);
    }

    @Test
    public void testMakeMoneyTransaction() {
        String msg = "";
        try {
            bank.makeMoneyTransaction(Long.valueOf(1), Long.valueOf(2), BigDecimal.valueOf(150));
        } catch (TransactionException | NoUserFoundException e) {
            msg = e.getMessage();
        }
        assertEquals("User 'John' has insufficient funds (100.00 < 150.00)", msg);

        bank.getUsers().get(Long.valueOf(1)).getWallet().setStatus(WalletStatus.BLOCKED);
        try {
            bank.makeMoneyTransaction(Long.valueOf(1), Long.valueOf(2), BigDecimal.valueOf(50));
        } catch (TransactionException | NoUserFoundException e) {
            msg = e.getMessage();
        }
        assertEquals("User 'John' wallet is blocked", msg);

        bank.getUsers().get(Long.valueOf(1)).getWallet().setStatus(WalletStatus.ACTIVE);
        try {
            bank.makeMoneyTransaction(Long.valueOf(2), Long.valueOf(1), BigDecimal.valueOf(950.507234));
        } catch (TransactionException | NoUserFoundException e) {
            msg = e.getMessage();
        }
        assertEquals("User 'John' wallet limit exceeded (100.00 + 950.50 > 1001.00)", msg);
    }
}
