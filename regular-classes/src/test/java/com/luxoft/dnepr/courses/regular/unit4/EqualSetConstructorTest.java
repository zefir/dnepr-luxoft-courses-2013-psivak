package com.luxoft.dnepr.courses.regular.unit4;


import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class EqualSetConstructorTest {

    @Test
    public void testConstructor() {
        EqualSet<Integer> set = new EqualSet(Arrays.asList(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8}));
        Assert.assertEquals(8, set.size());

        set = new EqualSet(Arrays.asList(new Integer[]{1, 2, 1, 1, 2, 2}));
        Assert.assertEquals(2, set.size());
    }

    @Test
    public void testConstructorWithNulls() {
        EqualSet<Integer> set = new EqualSet(Arrays.asList(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, null}));
        Assert.assertEquals(9, set.size());

        set = new EqualSet(Arrays.asList(new Integer[]{1, 2, 1, 1, 2, 2, null, null, null, null}));
        Assert.assertEquals(3, set.size());
    }
}
