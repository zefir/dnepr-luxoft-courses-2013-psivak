package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static junit.framework.Assert.*;

public class EqualSetTest {

    EqualSet<String> first = new EqualSet<>();
    EqualSet<String> second = new EqualSet<>();


    @Before
    public void setUp() {

        first.add("aaa");
        first.add("bbb");
        first.add("ccc");
        first.add("ddd");
        second.add("ddd");
        second.add("eee");
        second.add(null);
    }

    @Test
    public void testAdd() {
        assertEquals(4, first.size());
        assertEquals(1, first.getModificationCount());
        assertFalse(first.add("aaa"));
        assertTrue(first.add("xxx"));
        assertEquals(5, first.size());
        assertEquals(2, first.getModificationCount());
    }

    @Test
    public void testRemove() {
        assertEquals(1, first.getModificationCount());
        assertTrue(first.remove("aaa"));
        assertEquals(3, first.size());
        assertTrue(first.add("aaa"));
        assertEquals(4, first.size());
        assertEquals(2, first.getModificationCount());
        assertTrue(first.add(null));
        assertTrue(first.remove(null));
    }

    @Test
    public void testRetainAll() {
        assertTrue(first.retainAll(second));
        assertEquals(4, first.getModificationCount());
        assertEquals(1, first.size());
    }

    @Test
    public void testAddAll() {
        assertTrue(first.addAll(second));
        assertEquals(2, first.getModificationCount());
    }

    @Test
    public void testRemoveAll() {
        assertTrue(first.removeAll(second));
        assertFalse(first.removeAll(second));
        assertEquals(2, first.getModificationCount());
    }

    @Test
    public void testContains() {
        assertFalse(first.contains(null));
        first.add(null);
        assertTrue(first.contains(null));
    }

    @Test
    public void testToArray() {
        System.out.println(Arrays.toString(second.toArray()));

    }

}
