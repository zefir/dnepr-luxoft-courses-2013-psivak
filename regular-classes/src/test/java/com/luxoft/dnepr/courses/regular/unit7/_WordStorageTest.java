package com.luxoft.dnepr.courses.regular.unit7;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class _WordStorageTest {
    private WordStorage wordStorage;

    @Before
    public void setUp() {
        wordStorage = new WordStorage();
    }

    @After
    public void tearDown() {
        wordStorage = null;
    }

    @Test
    public void testSave() throws Exception {
        Thread t1 = new Thread(new MyRunnable("test"));
        Thread t2 = new Thread(new MyRunnable("test"));
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        assertEquals(2000, wordStorage.getWordStatistics().get("test"));
    }

    @Test
    public void testGetWordStatistics() {
        assertEquals(0, wordStorage.getWordStatistics().size());
        wordStorage.save("test");
        assertEquals(1, wordStorage.getWordStatistics().size());

        try {
            wordStorage.getWordStatistics().remove("test");
            fail("wordStatistics isn't unmodifiable");
        } catch (UnsupportedOperationException e) {}
    }

    class MyRunnable implements Runnable {

        private final String word;

        public MyRunnable(String word) {
            this.word = word;
        }

        @Override
        public void run() {
            for (int i = 0; i < 1000; i++) {
                wordStorage.save(word);
            }
        }
    }
}
