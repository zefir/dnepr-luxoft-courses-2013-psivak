package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

public class EqualSetIntegerTest {

    private Set<Integer> set = new EqualSet<Integer>();

    @Before
    public void init() {
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
    }

    @Test
    public void testSize() throws Exception {
        Assert.assertEquals(5, set.size());
    }

    @Test
    public void testIsEmpty() throws Exception {
        Assert.assertFalse(set.isEmpty());
        set.remove(1);
        Assert.assertFalse(set.isEmpty());
        set.remove(2);
        Assert.assertFalse(set.isEmpty());
        set.remove(3);
        Assert.assertFalse(set.isEmpty());
        set.remove(4);
        Assert.assertFalse(set.isEmpty());
        set.remove(5);
        Assert.assertTrue(set.isEmpty());
    }

    @Test
    public void testContains() throws Exception {
        Assert.assertTrue(set.contains(1));
        Assert.assertTrue(set.contains(3));
        Assert.assertTrue(set.contains(5));
        Assert.assertFalse(set.contains(7));
        Assert.assertFalse(set.contains(9));
        Assert.assertFalse(set.contains(11));
    }

    @Test
    public void testIterator() throws Exception {
        Iterator<Integer> iterator = set.iterator();
        List<Integer> actual = new ArrayList<Integer>();
        while (iterator.hasNext()) {
            Integer i = iterator.next();
            actual.add(i);
        }
        List<Integer> expected = new ArrayList<Integer>();
        expected.add(1);
        expected.add(2);
        expected.add(3);
        expected.add(4);
        expected.add(5);
        Assert.assertEquals(expected.size(), actual.size());
        Assert.assertTrue(expected.containsAll(actual) && actual.containsAll(expected));
    }

    @Test
    public void testToArray() throws Exception {
        Object[] actual = set.toArray();
        Assert.assertEquals(5, actual.length);
        List<Object> actualList = Arrays.asList(actual);
        Assert.assertTrue(actualList.contains(1));
        Assert.assertTrue(actualList.contains(2));
        Assert.assertTrue(actualList.contains(3));
        Assert.assertTrue(actualList.contains(4));
        Assert.assertTrue(actualList.contains(5));

    }

    @Test
    public void testToArray2() throws Exception {
        Integer[] actual = set.toArray(new Integer[]{});
        Assert.assertEquals(5, actual.length);
        List<Integer> actualList = Arrays.asList(actual);
        Assert.assertTrue(actualList.contains(1));
        Assert.assertTrue(actualList.contains(2));
        Assert.assertTrue(actualList.contains(3));
        Assert.assertTrue(actualList.contains(4));
        Assert.assertTrue(actualList.contains(5));
    }

    @Test
    public void testAdd() throws Exception {
        Assert.assertTrue(set.add(6));
        Assert.assertTrue(set.add(7));
        Assert.assertTrue(set.add(8));
        Assert.assertTrue(set.add(9));
        Assert.assertTrue(set.add(10));

        Assert.assertFalse(set.add(1));
        Assert.assertFalse(set.add(2));
        Assert.assertFalse(set.add(8));
        Assert.assertFalse(set.add(9));
        Assert.assertFalse(set.add(10));

        Assert.assertEquals(10, set.size());
    }

    @Test
    public void testAddNullValue() throws Exception {
        Assert.assertTrue(set.add(null));
        Assert.assertEquals(6, set.size());
        Assert.assertFalse(set.add(null));
        Assert.assertEquals(6, set.size());
        Assert.assertFalse(set.add(null));
        Assert.assertEquals(6, set.size());
    }

    @Test
    public void testRemove() throws Exception {
        Assert.assertTrue(set.remove(1));
        Assert.assertTrue(set.remove(2));
        Assert.assertFalse(set.remove(8));
        Assert.assertFalse(set.remove(9));
        Assert.assertFalse(set.remove(10));

        Assert.assertEquals(3, set.size());
    }

    @Test
    public void testRemoveNull() throws Exception {
        set.add(null);
        Assert.assertTrue(set.remove(null));
        Assert.assertFalse(set.remove(null));
        Assert.assertFalse(set.remove(null));
    }

    @Test
    public void testContainsAll() throws Exception {
        Assert.assertTrue(set.containsAll(Arrays.asList(new Integer[]{1, 2, 3, 4, 5})));
        Assert.assertTrue(set.containsAll(Arrays.asList(new Integer[]{1, 1, 1, 2, 3, 4, 5})));
        Assert.assertTrue(set.containsAll(Arrays.asList(new Integer[]{1, 2, 3, 3, 3, 3, 3, 4, 5})));

        Assert.assertTrue(set.containsAll(Arrays.asList(new Integer[]{1, 2, 3, 4})));
        Assert.assertTrue(set.containsAll(Arrays.asList(new Integer[]{1, 2, 3})));
        Assert.assertTrue(set.containsAll(Arrays.asList(new Integer[]{1, 2})));

        Assert.assertFalse(set.containsAll(Arrays.asList(new Integer[]{1, 2, 3, 4, 5, 6})));
        Assert.assertFalse(set.containsAll(Arrays.asList(new Integer[]{7})));
        Assert.assertFalse(set.containsAll(Arrays.asList(new Integer[]{15})));
    }

    @Test
    public void testAddAll() throws Exception {
        Set<Integer> expected = new HashSet<Integer>();
        expected.add(1);
        expected.add(2);
        expected.add(3);
        expected.add(4);
        expected.add(5);
        expected.add(6);
        set.addAll(Arrays.asList(new Integer[]{6}));
        assertSetEquals(expected, set);

        expected.add(7);
        set.addAll(Arrays.asList(new Integer[]{6, 7}));
        assertSetEquals(expected, set);

        expected.add(8);
        set.addAll(Arrays.asList(new Integer[]{1, 2, 3, 4, 5, 6, 6, 7, 8}));
        assertSetEquals(expected, set);
    }

    @Test
    public void testRetainAll() throws Exception {
        Set<Integer> expected = new HashSet<Integer>();
        expected.add(1);
        expected.add(2);
        expected.add(3);
        expected.add(4);
        expected.add(5);
        set.retainAll(Arrays.asList(new Integer[]{1, 2, 3, 4, 5, 6, 6, 7, 8}));
        assertSetEquals(expected, set);

        expected.remove(5);
        set.retainAll(Arrays.asList(new Integer[]{1, 2, 3, 4}));
        assertSetEquals(expected, set);

        expected.remove(4);
        set.add(5);
        set.retainAll(Arrays.asList(new Integer[]{1, 2, 3, 7}));
        assertSetEquals(expected, set);
        System.out.println("");
    }
    
    static class A {
    	
    }
    
    static class B extends A {
    	
    }

    @Test
    public void testRemoveAll() throws Exception {
        Set<Integer> expected = new HashSet<Integer>();
        expected.add(1);
        expected.add(2);
        expected.add(3);
        expected.add(4);
        set.removeAll(Arrays.asList(new Integer[]{5, 5, 5, 6, 6, null, null, null}));
    }

    @Test
    public void testClear() throws Exception {
        set.clear();
        Assert.assertTrue(set.isEmpty());
    }

    public static void assertSetEquals(Set setExpected, Set setActual) {
        Assert.assertEquals(setExpected.size(), setActual.size());
        Object[] arrayExpected = new Object[setExpected.size()];
        Object[] arrayActual = new Object[setActual.size()];
        int i = 0;
        for (Object o : setExpected) {
            arrayExpected[i] = o;
        }
        for (Object o : setActual) {
            arrayActual[i] = o;
        }
        Assert.assertArrayEquals(arrayExpected, arrayActual);
    }
}
