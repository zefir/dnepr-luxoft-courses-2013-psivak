package com.luxoft.dnepr.courses.regular.unit2;

public abstract class AbstractProduct implements Product,Cloneable{

    protected String code;

    protected String name;

    protected double price;

    public AbstractProduct(String code, String name, double price) {

        this.code = code;
        this.name = name;
        this.price = price;

    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public abstract int hashCode();

    @Override
    public abstract boolean equals(Object obj);

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return name;
    }
}
