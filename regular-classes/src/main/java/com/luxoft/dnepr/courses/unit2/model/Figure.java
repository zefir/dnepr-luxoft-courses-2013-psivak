package com.luxoft.dnepr.courses.unit2.model;


public abstract class Figure {

    public abstract double calculateArea();

    public void checkArgument(double arg) {
        if (arg < 0) {
            throw new IllegalArgumentException();
        }
    }

}
