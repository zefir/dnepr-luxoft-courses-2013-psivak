package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;

import java.io.*;
import java.util.Scanner;

public class PersonImpl implements Person {

    private String name;
    private String ethnicity;
    private Person father;
    private Person mother;
    private Gender gender;
    private int age;

    public PersonImpl() {

    }

    public PersonImpl(String name, String ethnicity, Person father, Person mother, Gender gender, int age) {
        this.name = name;
        this.ethnicity = ethnicity;
        this.father = father;
        this.mother = mother;
        this.gender = gender;
        this.age = age;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getEthnicity() {
        return ethnicity;
    }

    @Override
    public Person getFather() {
        return father;
    }

    @Override
    public Person getMother() {
        return mother;
    }

    @Override
    public Gender getGender() {
        return gender;
    }

    @Override
    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void save(OutputStream os) {

        PrintWriter writer = new PrintWriter(os);
        writer.print("{");
        if (name != null) writer.print("\"name\":\"" + getName() + "\",");
        if (gender != null) writer.print("\"gender\":\"" + getGender() + "\",");
        if (ethnicity != null) writer.print("\"ethnicity\":\"" + getEthnicity() + "\",");
        writer.print("\"age\":\"" + getAge() + "\"");
        if (father != null) {
            writer.print(",\"father\":");
            writer.flush();
            father.save(os);
        }
        if (mother != null) {
            writer.print(",\"mother\":");
            writer.flush();
            mother.save(os);
        }
        writer.print("}");
        writer.flush();

    }

    public void read(Scanner scanner) {

        while (scanner.hasNext()) {
            String[] el = scanner.next().split(":");
            if (el.length > 1) {
                setProperty(el[0].replaceAll("\"", ""), el[1].replaceAll("\"", ""));

            } else {
                String newPerson = el[0].replaceAll("\"", "");
                if (newPerson.equals("father")) {
                    this.father = new PersonImpl();
                    this.father.read(scanner);
                }
                if (newPerson.equals("mother")) {
                    mother = new PersonImpl();
                    mother.read(scanner);
                }
                if (newPerson.equals("")) {
                    return;
                }
            }
        }
    }

    @Override
    public void setProperty(String propertyName, String propertyValue) {
        switch (propertyName) {
            case "name":
                setName(propertyValue);
                break;
            case "ethnicity":
                setEthnicity(propertyValue);
                break;
            case "gender":
                setGender(Gender.valueOf(propertyValue));
                break;
            case "age":
                setAge(Integer.parseInt(propertyValue));
                break;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        if (name != null) sb.append("\"name\":\"" + getName() + "\",");
        if (gender != null) sb.append("\"gender\":\"" + getGender() + "\",");
        if (ethnicity != null) sb.append("\"ethnicity\":\"" + getEthnicity() + "\",");
        if (age > 0) sb.append("\"age\":\"" + getAge() + "\"");
        if (father != null) {
            sb.append(",\"father\":");
            sb.append(father.toString());
        }
        if (mother != null) {
            sb.append(",\"mother\":");
            sb.append(mother.toString());
        }
        sb.append("}");
        return sb.toString();

    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(out);
        writer.write(this.toString());
        writer.flush();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        Scanner scanner = new Scanner(in);
        scanner.useDelimiter("[{},]");
        while (scanner.hasNext()) {
            String[] el = scanner.next().split(":");
            if (el.length > 1) {
                setProperty(el[0].replaceAll("\"", ""), el[1].replaceAll("\"", ""));

            } else {
                String newPerson = el[0].replaceAll("\"", "");
                if (newPerson.equals("father")) {
                    this.father = new PersonImpl();
                    father.read(scanner);
                }
                if (newPerson.equals("mother")) {
                    this.mother = new PersonImpl();
                    mother.read(scanner);
                }
                if (newPerson.equals("")) {
                    return;
                }
            }
        }
    }
}
