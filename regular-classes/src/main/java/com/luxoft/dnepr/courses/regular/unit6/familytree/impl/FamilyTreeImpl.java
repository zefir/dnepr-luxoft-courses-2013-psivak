package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class FamilyTreeImpl implements FamilyTree {

    private static final long serialVersionUID = 3057396458981676327L;
    private Person root;
    private transient long creationTime;

    private FamilyTreeImpl(Person root, long creationTime) {
        this.root = root;
        this.creationTime = creationTime;
    }

    public static FamilyTree create(Person root) {
        return new FamilyTreeImpl(root, System.currentTimeMillis());
    }

    @Override
    public Person getRoot() {
        return root;
    }

    @Override
    public long getCreationTime() {
        return creationTime;
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(out);
        writer.write("{\"root\":");
        writer.write(root.toString());
        writer.write("}");
        writer.flush();
    }

    private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException {

        Scanner scanner = new Scanner(is);
        scanner.useDelimiter("[{},]");
        scanner.next();
        Person person = new PersonImpl();
        if (scanner.hasNext()) {
            this.root = person;
        } else {
            return;
        }
        while (scanner.hasNext()) {
            String[] el = scanner.next().split(":");
            if (el.length > 1) {
                person.setProperty(el[0].replaceAll("\"", ""), el[1].replaceAll("\"", ""));

            } else {
                String newPerson = el[0].replaceAll("\"", "");
                if (newPerson.equals("father")) {
                    person.setFather(new PersonImpl());
                    person.getFather().read(scanner);
                }
                if (newPerson.equals("mother")) {
                    person.setMother(new PersonImpl());
                    person.getMother().read(scanner);
                }
                if (newPerson.equals("")) {
                    return;
                }
            }
        }
    }
}
