package com.luxoft.dnepr.courses.regular.unit4;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class EqualSet<E> implements Set<E> {

    private static final int INITIAL_CAPACITY = 2;

    private Object[] elementData;

    private int size;

    private int modificationCount;

    public EqualSet() {
        elementData = new Object[INITIAL_CAPACITY];
    }

    public EqualSet(Collection<? extends E> collection) {
        this();
        this.addAll(collection);
    }

    public int getModificationCount() {
        return modificationCount;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < size; i++) {
            sb.append(elementData[i]);
            if (i != size - 1) {
                sb.append(", ");
            } else {
                sb.append("]");
            }
        }
        return sb.toString();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (elementData[i] == null) {
                    return true;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (elementData[i].equals(o)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return new Itr<E>();
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(elementData, size);
    }

    @Override
    public <T> T[] toArray(T[] a) {
        if (a.length < size) {
            return (T[]) Arrays.copyOf(elementData, size, a.getClass());
        }
        System.arraycopy(elementData, 0, a, 0, size);
        if (a.length > size)
            a[size] = null;
        return a;
    }

    @Override
    public boolean add(E e) {
        checkSize();
        if (e == null) {
            for (int i = 0; i < size; i++) {
                if (elementData[i] == null) {
                    return false;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (elementData[i].equals(e)) {
                    return false;
                }
            }
        }
        elementData[size++] = e;
        return true;
    }

    private void checkSize() {
        if (elementData.length == size) {
            increaseArray();
        }
    }

    private void increaseArray() {
        int newSize = size << 1;
        elementData = Arrays.copyOf(elementData, newSize);
        modificationCount++;
    }

    @Override
    public boolean remove(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (elementData[i] == null) {
                    dropElement(i);
                    return true;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (elementData[i].equals(o)) {
                    dropElement(i);
                    return true;
                }
            }
        }
        return false;
    }

    private void dropElement(int index) {
        modificationCount++;
        int moveNumber = size - index - 1;
        if (moveNumber > 0) {
            System.arraycopy(elementData, index + 1, elementData, index, moveNumber);
        }
        elementData[--size] = null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            boolean ifExist = false;
            if (o == null) {
                for (int i = 0; i < size; i++) {
                    if (elementData[i] == null) {
                        ifExist = true;
                        break;
                    }
                }
            } else {
                for (int i = 0; i < size; i++) {
                    if (elementData[i].equals(o)) {
                        ifExist = true;
                        break;
                    }
                }
            }
            if (!ifExist) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        int initialSize = size;
        for (E e : c) {
            add(e);
        }
        return initialSize != size;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        int initialSize = size;
        for (int i = 0; i < size; i++) {
            boolean ifExist = false;
            for (Object o : c) {
                if (o == null) {
                    if (elementData[i] == null) {
                        ifExist = true;
                        break;
                    }

                } else if (elementData[i].equals(o)) {
                    ifExist = true;
                    break;
                }
            }
            if (!ifExist) {
                remove(elementData[i--]);
            }
        }
        return initialSize != size;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        int initialSize = size;
        for (Object element : c) {
            remove(element);
        }
        return initialSize != size;
    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            elementData[i] = null;
        }
        size = 0;
    }

    private class Itr<E> implements Iterator<E> {

        private int cursor;
        private int previous = -1;

        @Override
        public boolean hasNext() {
            return cursor < size;
        }

        @Override
        public E next() {
            previous = cursor;
            return (E) elementData[cursor++];
        }

        @Override
        public void remove() {
            dropElement(previous--);
            cursor--;
        }
    }

}
