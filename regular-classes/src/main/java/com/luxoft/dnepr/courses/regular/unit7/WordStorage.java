package com.luxoft.dnepr.courses.regular.unit7;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class WordStorage {

    Map<String, Integer> storage;

    public WordStorage() {
        storage = new HashMap<>();
    }

    public void save(String word) {
        if (storage.containsKey(word)) {
            synchronized (this) {
                Integer number = storage.get(word);
                number++;
                storage.put(word, number);
            }
        } else {
            synchronized (this) {
                storage.put(word, 1);
            }
        }
    }

    /**
     * @return unmodifiable map containing words and corresponding counts of occurrences.
     */
    public Map<String, ? extends Number> getWordStatistics() {
        return Collections.unmodifiableMap(storage);
    }
}
