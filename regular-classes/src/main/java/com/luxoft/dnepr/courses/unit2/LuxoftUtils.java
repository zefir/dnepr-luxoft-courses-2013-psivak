package com.luxoft.dnepr.courses.unit2;


import com.luxoft.dnepr.courses.unit2.model.Figure;

import java.util.*;

public class LuxoftUtils {

    private LuxoftUtils() {
    }

    public static String[] sortArray(String[] array, boolean asc) {
        if (array == null) {
            throw new IllegalArgumentException();
        }
        if (array.length == 1 || array.length == 0) {
            return array;
        }

        return sort(array, 0, array.length, asc);

    }

    private static String[] sort(String[] array, int from, int len, boolean asc) {

        if (len == 0) {
            return new String[0];
        } else if (len == 1) {
            return new String[]{array[from]};
        } else if (len == 2) {
            if (compare(array[from], array[from + 1], asc)) {
                return new String[]{array[from], array[from + 1]};
            } else {
                return new String[]{array[from + 1], array[from]};
            }
        } else {
            String[] left = sort(array, from, len / 2, asc);
            String[] right = sort(array, from + (len / 2), len - (len / 2), asc);
            return merge(left, right, asc);
        }

    }

    private static boolean compare(String s1, String s2, boolean asc) {
        return (!(s1.compareTo(s2) <= 0) ^ asc);
    }

    private static String[] merge(String[] left, String[] right, boolean asc) {
        String[] result = new String[left.length + right.length];
        int leftIndex = 0;
        int rightIndex = 0;
        while (leftIndex + rightIndex != result.length) {
            if (leftIndex == left.length) {
                System.arraycopy(right, rightIndex, result, rightIndex + leftIndex, right.length - rightIndex);
                break;
            }
            if (rightIndex == right.length) {
                System.arraycopy(left, leftIndex, result, rightIndex + leftIndex, left.length - leftIndex);
                break;
            }
            if (compare(left[leftIndex], right[rightIndex], asc)) {
                result[leftIndex + rightIndex] = left[leftIndex++];
            } else {
                result[leftIndex + rightIndex] = right[rightIndex++];
            }
        }
        return result;
    }

    public static double wordAverageLength(String str) {

        if (str == null) {
            throw new IllegalArgumentException();
        }

        if (str.length() == 0) {
            return 0;
        }
        str = getStringWithoutSpaces(str);
        String[] strArray = str.split(" ");
        if (strArray.length == 1) {
            return str.length();
        }
        return (double) (str.length() - strArray.length + 1) / strArray.length;

    }

    public static String reverseWords(String str) {
        if (str == null) {
            throw new IllegalArgumentException();
        }

        String[] strArray = getStringWithoutSpaces(str).split(" ");
        for (String s : strArray) {
            str = str.replace(s, new StringBuilder(s).reverse());
        }
        return str;
    }

    private static String getStringWithoutSpaces(String str) {
        return str.trim().replaceAll(" +", " ");
    }

    public static char[] getCharEntries(String str) {

        TreeMap<Character, Integer> map = new TreeMap<>();
        SortedSet<Map.Entry<Character, Integer>> sortedSet = new TreeSet<>(new Comparator<Map.Entry<Character, Integer>>() {
            @Override
            public int compare(Map.Entry<Character, Integer> o1, Map.Entry<Character, Integer> o2) {
                if (o1.getValue().equals(o2.getValue())) {
                    return o1.getKey().compareTo(o2.getKey());
                }
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        fillMapWithCharEntries(map, str);
        sortedSet.addAll(map.entrySet());
        char[] chars = new char[sortedSet.size()];
        int i = 0;

        for (Map.Entry<Character, Integer> e : sortedSet) {
            chars[i++] = e.getKey();
        }

        return chars;
    }

    private static void fillMapWithCharEntries(TreeMap<Character, Integer> map, String str) {
        for (char c : str.replaceAll(" +", "").toCharArray()) {
            if (map.get(c) == null) {
                map.put(c, 1);
            } else {
                map.put(c, map.get(c) + 1);
            }
        }
    }

    public static double calculateOverallArea(List<Figure> figures) {

        if (figures == null) {
            throw new IllegalArgumentException();
        }

        double result = 0;
        for (Figure figure : figures) {
            result += figure.calculateArea();
        }
        return result;
    }

}
