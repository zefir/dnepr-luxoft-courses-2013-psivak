package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

import java.util.HashMap;
import java.util.Map;

public class EntityStorage {

    private final static Map<Long, Entity> entities = new HashMap();

    private EntityStorage() {

    }

    public static Map<Long, Entity> getEntities() {
        return entities;
    }

    public static Long getMaxId() {
        Long maxId = Long.valueOf(0);
        for (Long key : entities.keySet()) {
            if (key > maxId) {
                maxId = key;
            }
        }
        return maxId;
    }

    public static boolean ifIdExist(Long id) {
        for (Long currentId : entities.keySet()) {
            if (currentId == id) {
                return true;
            }
        }
        return false;
    }
}
