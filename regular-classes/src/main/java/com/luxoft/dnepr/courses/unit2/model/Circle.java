package com.luxoft.dnepr.courses.unit2.model;

public class Circle extends Figure {

    private final double radius;

    public Circle(double radius) {
        checkArgument(radius);
        this.radius = radius;
    }

    @Override
    public double calculateArea() {
        return Math.PI * Math.pow(radius, 2);
    }
}
