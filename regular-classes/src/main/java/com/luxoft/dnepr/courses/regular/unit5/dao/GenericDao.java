package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import static com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage.getEntities;
import static com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage.ifIdExist;

public abstract class GenericDao<T extends Entity> implements IDao<T> {

    private Class<T> type;

    public GenericDao(Class<T> type) {
        this.type = type;
    }

    @Override
    public T save(T entity) throws UserAlreadyExist {
        if (entity.getId() == null) {
            entity.setId(EntityStorage.getMaxId() + 1);
        }
        if (ifIdExist(entity.getId())) {
            throw new UserAlreadyExist();
        }

        getEntities().put(entity.getId(), entity);
        return entity;
    }


    @Override
    public T update(T entity) throws UserNotFound {

        if (entity.getId() == null || !ifIdExist(entity.getId())) {
            throw new UserNotFound();
        }
        getEntities().put(entity.getId(), entity);
        return entity;

    }

    @Override
    public T get(long id) {
        T element = (T) getEntities().get(id);
        if (element.getClass() != type) {
            return null;
        }
        return element;
    }


    @Override
    public boolean delete(long id) {
        if (id < 0 || !ifIdExist(id)) {
            return false;
        }
        getEntities().remove(id);
        return true;
    }

    public T create() {
        try {
            return type.getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            return null;
        }
    }
}
