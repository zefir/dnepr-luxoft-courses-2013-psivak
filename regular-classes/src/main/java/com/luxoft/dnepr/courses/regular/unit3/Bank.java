package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.util.Map;

public class Bank implements BankInterface {

    private static final int ROUND_FORMAT = BigDecimal.ROUND_DOWN;

    private Map<Long, UserInterface> users;

    private UserInterface currentUser;

    public Bank(String expectedJavaVersion) {

        String actualJavaVersion = System.getProperty("java.version");
        if (expectedJavaVersion == null || expectedJavaVersion.length() == 0 || !actualJavaVersion.startsWith(expectedJavaVersion)) {
            throw new IllegalJavaVersionError(actualJavaVersion, expectedJavaVersion, "Wrong Java version");
        }
    }

    @Override
    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    @Override
    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;
    }

    @Override
    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {
        UserInterface fromUser = users.get(fromUserId);
        UserInterface toUser = users.get(toUserId);

        if (fromUser == null || toUser == null) {
            Long currentUserId = toUser == null ? toUserId : fromUserId;
            throw new NoUserFoundException(currentUserId, getUserNotFoundError(currentUserId));
        }

        currentUser = fromUser;
        try {
            currentUser.getWallet().checkWithdrawal(amount);
            currentUser = toUser;
            currentUser.getWallet().checkTransfer(amount);
        } catch (WalletIsBlockedException e) {
            throw new TransactionException(getWalletIsBlockedError(currentUser));
        } catch (InsufficientWalletAmountException e) {
            throw new TransactionException(getInsifficientWalletAmountError(currentUser, amount));
        } catch (LimitExceededException e) {
            throw new TransactionException(getLimitWalletExceededAmountError(currentUser, amount));
        }

        fromUser.getWallet().withdraw(amount);
        toUser.getWallet().transfer(amount);
    }

    private String getLimitWalletExceededAmountError(UserInterface currentUser, BigDecimal amount) {
        return "User '" + currentUser.getName() + "' wallet limit exceeded (" + currentUser.getWallet().getAmount().setScale(2, ROUND_FORMAT) + " + " + amount.setScale(2, ROUND_FORMAT) + " > " + currentUser.getWallet().getMaxAmount().setScale(2, ROUND_FORMAT) + ")";
    }

    private String getInsifficientWalletAmountError(UserInterface currentUser, BigDecimal amount) {
        return "User '" + currentUser.getName() + "' has insufficient funds (" + currentUser.getWallet().getAmount().setScale(2, ROUND_FORMAT) + " < " + amount.setScale(2, ROUND_FORMAT) + ")";
    }

    private String getWalletIsBlockedError(UserInterface currentUser) {
        return "User '" + currentUser.getName() + "' wallet is blocked";
    }

    private String getUserNotFoundError(Long currentUserId) {
        return "User with id" + currentUserId + "not found";
    }
}
