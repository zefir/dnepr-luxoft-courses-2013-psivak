package com.luxoft.dnepr.courses.regular.unit2;

public class Beverage extends AbstractProduct{

    private boolean isNonAlcoholic;

    public Beverage(String code, String name, double price, boolean nonAlcoholic) {
        super(code, name, price);
        isNonAlcoholic = nonAlcoholic;
    }

    public boolean isNonAlcoholic() {
        return isNonAlcoholic;
    }

    public void setNonAlcoholic(boolean nonAlcoholic) {
        isNonAlcoholic = nonAlcoholic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Beverage beverage = (Beverage) o;

        if (isNonAlcoholic != beverage.isNonAlcoholic) return false;
        if (!code.equals(beverage.code)) return false;
        if (!name.equals(beverage.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + (isNonAlcoholic ? 1 : 0);
        return result;
    }
}
