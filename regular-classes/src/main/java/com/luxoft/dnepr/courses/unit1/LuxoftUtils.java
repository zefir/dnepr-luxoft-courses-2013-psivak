package com.luxoft.dnepr.courses.unit1;

import java.text.DateFormatSymbols;
import java.util.Arrays;
import java.util.Locale;

public class LuxoftUtils {

    private static String WRONG_MONTH_EN = "Unknown Month";

    private static String WRONG_MONTH_RU = "Неизвестный месяц";

    private static String WRONG_LANGUAGE = "Unknown Language";

    private static String WRONG_BINARY = "Not binary";

    private static String WRONG_DECIMAL = "Not decimal";

    private LuxoftUtils() {

    }

    public static String getMonthName(int monthOrder, String language) {

        String currentMonthLanguageError;
        switch (language) {
            case "en":
                currentMonthLanguageError = WRONG_MONTH_EN;
                break;
            case "ru":
                currentMonthLanguageError = WRONG_MONTH_RU;
                break;
            default:
                return WRONG_LANGUAGE;
        }
        if (monthOrder > 12 || monthOrder < 1)
            return currentMonthLanguageError;
        String[] monthNames = new DateFormatSymbols(Locale.forLanguageTag(language)).getMonths();

        return monthNames[monthOrder - 1];

    }

    public static String binaryToDecimal(String binaryNumber) {

        int decimalNumber = 0;
        if (binaryNumber == null || binaryNumber.length() == 0)
            return WRONG_BINARY;
        for (int i = binaryNumber.length() - 1; i >= 0; i--) {

            if (binaryNumber.charAt(i) != '0' && binaryNumber.charAt(i) != '1')
                return WRONG_BINARY;
            decimalNumber += Integer.parseInt(binaryNumber.substring(i, i + 1)) * Math.pow(2, binaryNumber.length() - i - 1);
        }
        return String.valueOf(decimalNumber);

    }

    public static String decimalToBinary(String decimalNumber) {

        int decimal;
        StringBuilder result = new StringBuilder();
        try {
            decimal = Integer.parseInt(decimalNumber);
        } catch (NumberFormatException e) {
            return WRONG_DECIMAL;
        }
        if (decimal < 0) return WRONG_DECIMAL;

        while (decimal > 1) {

            result.append(decimal % 2);
            decimal = decimal / 2;
        }

        result.append(decimal);

        return result.reverse().toString();
    }

    public static int[] sortArray(int[] array, boolean asc) {

        if (array == null) return null;

            int[] sortedArray = Arrays.copyOf(array, array.length);

        for (int i = 0; i < sortedArray.length - 1; i++) {
            for (int k = 0; k < sortedArray.length - i - 1; k++) {
                if (sortedArray[k] > sortedArray[k + 1]) {
                    swapElements(sortedArray, k, k+1);
                }
            }
        }

        if (asc) {
            return sortedArray;
        }

        for (int i = 0; i < sortedArray.length / 2; i++) {
            swapElements(sortedArray, i, sortedArray.length - i - 1);
        }
        return sortedArray;
    }

    private static void swapElements(int[] array, int leftIndex, int rightIndex) {

        int k = array[leftIndex];
        array[leftIndex] = array[rightIndex];
        array[rightIndex] = k;

    }

}
