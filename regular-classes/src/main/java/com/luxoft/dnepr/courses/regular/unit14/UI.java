package com.luxoft.dnepr.courses.regular.unit14;

import java.util.Scanner;


public class UI implements Runnable {

    private Controller controller = new Controller();

    public void run() {

        Scanner scanner = new Scanner(System.in);
        boolean isExit = false;
        controller.processUserInput("help", null);
        System.out.println("Vocabulary size is - " + controller.getVocabulary().getSize() + " words");
        while (!isExit) {
            String randomWord = controller.getVocabulary().random();
            System.out.println("Do you know translation of this word?\n" + randomWord);
            isExit = controller.processUserInput(scanner.next(), randomWord);
        }

    }

    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(new UI());
        t.start();
        t.join();
    }
}
