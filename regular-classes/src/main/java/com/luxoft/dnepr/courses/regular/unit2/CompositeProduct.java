package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents group of similar {@link Product}s.
 * Implementation of pattern Composite.
 */
public class CompositeProduct implements Product {
    private List<Product> childProducts = new ArrayList<Product>();

    public CompositeProduct() {

    }

    public CompositeProduct(Product localProduct, Product newProduct) {
        childProducts.add(localProduct);
        childProducts.add(newProduct);
    }

    /**
     * Returns code of the first "child" product or null, if child list is empty
     *
     * @return product code
     */
    @Override
    public String getCode() {
        throw new UnsupportedOperationException("getCode");
    }

    /**
     * Returns name of the first "child" product or null, if child list is empty
     *
     * @return product name
     */
    @Override
    public String getName() {
        throw new UnsupportedOperationException("getName");
    }

    /**
     * Returns total price of all the child products taking into account discount.
     * 1 item - no discount
     * 2 items - 5% discount
     * >= 3 items - 10% discount
     *
     * @return total price of child products
     */
    @Override
    public double getPrice() {
        double sum = 0;
        for (Product product : childProducts) {
            sum += product.getPrice();
        }
        if (getAmount() >= 3) {
            sum -= sum * 0.1;
        } else if (getAmount() > 1) {
            sum -= sum * 0.05;
        }
        return sum;
    }

    public int getAmount() {
        return childProducts.size();
    }

    public Product getFirst() {
        return childProducts.get(0);
    }

    public void add(Product product) {
        childProducts.add(product);
    }

    public void remove(Product product) {
        childProducts.remove(product);
    }

    @Override
    public String toString() {
        return getName() + " * " + getAmount() + " = " + getPrice();
    }

    @Override
    public boolean equals(Object o) {
        return childProducts.get(0).equals(o);
    }


}
