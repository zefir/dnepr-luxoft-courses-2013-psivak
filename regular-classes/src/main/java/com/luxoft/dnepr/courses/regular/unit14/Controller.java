package com.luxoft.dnepr.courses.regular.unit14;

import java.util.HashMap;
import java.util.Map;

public class Controller {

    private final String yesInput = "y";
    private final String noInput = "n";
    private final String helpInput = "help";
    private final String exitInput = "exit";
    private final String errorInput = "Error Input. Try again";

    private Words words = new Words();

    private Vocabulary vocabulary = new Vocabulary();

    private Map<String, Action> actionMap = new HashMap<>();

    public Words getWords() {
        return words;
    }

    public Vocabulary getVocabulary() {
        return vocabulary;
    }

    public Controller() {
        actionMap.put(helpInput, new HelpAction());
        actionMap.put(yesInput, new YesAction(this));
        actionMap.put(noInput, new NoAction(this));
    }

    public boolean processUserInput(String userInput, String randomWord) {

        if (userInput.equalsIgnoreCase(exitInput)) {
            words.printResult(vocabulary.getSize());
            return true;
        }
        if (actionMap.containsKey(userInput.toLowerCase())) {
            actionMap.get(userInput).doAction(randomWord);
        } else {
            System.out.println(errorInput);
        }
        return false;
    }

}
