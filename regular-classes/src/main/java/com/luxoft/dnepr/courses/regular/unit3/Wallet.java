package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;

public class Wallet implements WalletInterface {

    private Long id;

    private BigDecimal amount;

    private BigDecimal maxAmount;

    private WalletStatus status;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public WalletStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    @Override
    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    @Override
    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    @Override
    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {
        if (status == WalletStatus.BLOCKED) {
            throw new WalletIsBlockedException(id, "Wallet is BLOCKED");
        }
        if (amountToWithdraw.compareTo(amount) > 0) {
            throw new InsufficientWalletAmountException(id, amountToWithdraw, amount, "You don't have enough money");
        }

    }

    @Override
    public void withdraw(BigDecimal amountToWithdraw) {
        amount = amount.subtract(amountToWithdraw);
    }

    @Override
    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        if (status == WalletStatus.BLOCKED) {
            throw new WalletIsBlockedException(id, "Wallet is blocked");
        }
        BigDecimal sum = amountToTransfer.add(amount);
        if (sum.compareTo(maxAmount) > 0) {
            throw new LimitExceededException(id, amountToTransfer, amount, "Limit Exceeded");
        }
    }

    @Override
    public void transfer(BigDecimal amountToTransfer) {
        amount = amount.add(amountToTransfer);
    }
}
