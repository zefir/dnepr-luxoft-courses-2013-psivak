package com.luxoft.dnepr.courses.regular.unit14;

public class NoAction implements Action {

    private Controller controller;

    public NoAction(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void doAction(String userInput) {
        controller.getWords().addWord(userInput, false);
    }
}
