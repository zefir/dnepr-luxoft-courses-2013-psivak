package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;

import java.io.*;

public class IOUtils {
	
	private IOUtils() {
	}
	
	public static FamilyTree load(String filename) throws IOException, ClassNotFoundException {
		return load(new FileInputStream(filename));
	}
	
	public static FamilyTree load(InputStream is) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(is);
        FamilyTree familyTree = (FamilyTree) ois.readObject();
        is.close();
        return familyTree;
	}
	
	public static void save(String filename, FamilyTree familyTree) throws IOException {
		save(new FileOutputStream(filename), familyTree);
	}
	
	public static void save(OutputStream os, FamilyTree familyTree) throws IOException {
        ObjectOutputStream outputStream = new ObjectOutputStream(os);
        outputStream.writeObject(familyTree);
        os.flush();
        os.close();
	}
}
