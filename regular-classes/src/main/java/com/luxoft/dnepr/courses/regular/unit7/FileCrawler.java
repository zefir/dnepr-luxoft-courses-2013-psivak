package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Crawls files and directories, searches for text files and counts word occurrences.
 */
public class FileCrawler {

    private final String FILE_EXTENSION = "txt";

    private final int maxNumberOfThreads;

    private WordStorage wordStorage = new WordStorage();

    private String rootFolder;

    public FileCrawler(String rootFolder, int maxNumberOfThreads) {
        this.rootFolder = rootFolder;
        this.maxNumberOfThreads = maxNumberOfThreads;
    }

    /**
     * Performs crawling using multiple threads.
     * This method should wait until all parallel tasks are finished.
     *
     * @return FileCrawlerResults
     */

    public FileCrawlerResults execute() {
        BlockingQueue<File> queueOfFiles = new LinkedBlockingQueue<>();
        List<File> processedFiles = new ArrayList<>();
        FileProducer producer = new FileProducer(FILE_EXTENSION, rootFolder, queueOfFiles);
        FileConsumer consumer = new FileConsumer(queueOfFiles, processedFiles, wordStorage);
        Thread[] threads = getArrayOfThreads(maxNumberOfThreads, producer, consumer);

        startThreads(threads);

        try {
            waitThreads(threads);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return new FileCrawlerResults(processedFiles, wordStorage.getWordStatistics());

    }

    private void waitThreads(Thread[] threads) throws InterruptedException {
        for (Thread thread : threads) {
            thread.join();
        }
    }

    private void startThreads(Thread[] threads) {
        for (Thread thread : threads) {
            thread.start();
        }
    }

    private Thread[] getArrayOfThreads(int maxNumberOfThreads, FileProducer producer, FileConsumer consumer) {
        Thread[] threads = new Thread[maxNumberOfThreads];
        threads[0] = new Thread(producer);
        for (int i = 1; i < maxNumberOfThreads; i++) {
            threads[i] = new Thread(consumer);
        }
        return threads;
    }

    public static void main(String args[]) {
        String rootDir = args.length > 0 ? args[0] : System.getProperty("user.home");

        FileCrawler crawler = new FileCrawler(rootDir, 10);
        FileCrawlerResults results = crawler.execute();
        Map<String, ? extends Number> map = results.getWordStatistics();
        for (Map.Entry<String, ? extends Number> entry : map.entrySet()) {
            System.out.println(entry.getKey() + ":" + entry.getValue());
        }
        for (File f : results.getProcessedFiles()) {
            System.out.println(f.getName());
        }
    }

}
