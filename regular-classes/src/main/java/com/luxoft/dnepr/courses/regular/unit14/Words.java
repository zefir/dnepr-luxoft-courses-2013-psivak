package com.luxoft.dnepr.courses.regular.unit14;

import java.util.HashSet;
import java.util.Set;

public class Words {
    public Set<String> known = new HashSet<>();
    public Set<String> unknown = new HashSet<>();

    public void printResult(int total) {
        int w = total * (known.size() + 1) / (known.size() + unknown.size() + 1);
        System.out.println("Your estimated vocabulary is " + w + " words");
    }

    public void addWord(String word, Boolean isKnown) {
        if (isKnown) {
            known.add(word);
        } else {
            unknown.add(word);
        }
    }

}
