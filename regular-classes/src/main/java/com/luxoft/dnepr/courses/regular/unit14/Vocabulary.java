package com.luxoft.dnepr.courses.regular.unit14;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;

public class Vocabulary {
    private String fileName = "sonnets.txt";
    private HashSet<String> vocabulary = new HashSet<>();

    public Vocabulary() {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/unit14/" + fileName)))) {
            while (reader.ready()) {
                String line = reader.readLine();
                if (!line.isEmpty()) {
                    addWordsToVocabulary(line);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Read File Error", e);
        }

    }

    private void addWordsToVocabulary(String line) {
        String[] wordsArray = line.split("\\W+");
        for (String str : wordsArray) {
            if (str.length() > 3) {
                vocabulary.add(str.toLowerCase());
            }
        }
    }

    public String random() {
        return (String) vocabulary.toArray()[(int) (Math.random() * vocabulary.size())];
    }

    public int getSize() {
        return vocabulary.size();
    }
}
