package com.luxoft.dnepr.courses.regular.unit2;

public class Bread extends AbstractProduct {

    private double weight;

    public Bread(String code, String name, double price, double weight) {

        super(code, name, price);
        this.weight = weight;

    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bread bread = (Bread) o;

        if (Double.compare(bread.weight, weight) != 0) return false;
        if (code != null ? !code.equals(bread.code) : bread.code != null) return false;
        if (name != null ? !name.equals(bread.name) : bread.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = code != null ? code.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        temp = Double.doubleToLongBits(weight);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
