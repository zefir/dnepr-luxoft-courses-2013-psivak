package com.luxoft.dnepr.courses.regular.unit14;

public class YesAction implements Action {
    private final Controller controller;

    public YesAction(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void doAction(String userInput) {
        controller.getWords().addWord(userInput, true);
    }
}
