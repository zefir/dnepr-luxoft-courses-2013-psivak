package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.io.FilenameFilter;
import java.util.concurrent.BlockingQueue;

public class FileProducer implements Runnable {

    private final BlockingQueue<File> queueOfFiles;

    private final String rootFolder;

    private final String fileExtension;

    public static final File STUB = new File("");

    public FileProducer(String fileExtension, String rootFolder, BlockingQueue<File> queueOfFiles) {
        this.fileExtension = fileExtension;
        this.rootFolder = rootFolder;
        this.queueOfFiles = queueOfFiles;
    }

    @Override
    public void run() {
        findFiles(rootFolder);
        try {
            queueOfFiles.put(STUB);
        } catch (InterruptedException e) {

        }
    }

    private void findFiles(String rootFolder) {

        File temp = new File(rootFolder);
        File[] fileList = temp.listFiles(new DirectoryAndExtensionFilter(fileExtension));
        if (fileList == null) {
            return;
        }
        for (File file : fileList) {
            if (file.isDirectory()) {
                findFiles(file.getPath());
            } else {
                try {
                    queueOfFiles.put(file);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class DirectoryAndExtensionFilter implements FilenameFilter {

        private String extension;

        private DirectoryAndExtensionFilter(String extension) {
            this.extension = "." + extension;
        }

        @Override
        public boolean accept(File dir, String name) {

            return new File(dir, name).isDirectory() || name.endsWith(extension);
        }
    }
}
