package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;

public class FileConsumer implements Runnable {

    private final BlockingQueue<File> queueOfFiles;

    private final List<File> processedFiles;

    private final WordStorage wordStorage;

    public FileConsumer(BlockingQueue<File> queueOfFiles, List<File> processedFiles, WordStorage wordStorage) {
        this.queueOfFiles = queueOfFiles;
        this.processedFiles = processedFiles;
        this.wordStorage = wordStorage;
    }

    @Override
    public void run() {
        try {
            boolean isLast = false;
            while (!Thread.currentThread().isInterrupted() && !isLast) {
                File f = queueOfFiles.take();
                if (f == FileProducer.STUB) {
                    queueOfFiles.put(f);
                    isLast = true;
                } else {
                    crawl(f);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void crawl(File file) {
        processedFiles.add(file);
        try (FileInputStream fis = new FileInputStream(file)) {
            Scanner scanner = new Scanner(fis, "UTF-8");
            /*
            * Solving problem with searching words - cyrillic and latin
            * because \W isn't include cyrillic symbols
            * pattern [\\p{Punct}\\p{Space}] isn't full
            * http://stackoverflow.com/questions/4304928/unicode-equivalents-for-w-and-b-in-java-regular-expressions
            * */
            scanner.useDelimiter("[^\\pL\\pM\\p{Nd}\\p{Nl}\\p{Pc}[\\p{InEnclosedAlphanumerics}&&\\p{So}]]");

            while (scanner.hasNext()) {
                String word = scanner.next();
                if (!word.equals(""))
                    wordStorage.save(word);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
