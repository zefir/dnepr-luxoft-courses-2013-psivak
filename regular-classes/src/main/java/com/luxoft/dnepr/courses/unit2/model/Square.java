package com.luxoft.dnepr.courses.unit2.model;


public class Square extends Figure {

    private final double side;

    public Square(double side) {
        checkArgument(side);
        this.side = side;
    }

    @Override
    public double calculateArea() {
        return side * side;
    }
}
