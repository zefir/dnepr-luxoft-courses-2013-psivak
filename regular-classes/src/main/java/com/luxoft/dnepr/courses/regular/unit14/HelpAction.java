package com.luxoft.dnepr.courses.regular.unit14;

public class HelpAction implements Action {
    public static String HELP_MESSAGE = "Linguistic analizator v1, autor Tushar Brahmacobalol. \n Please, type help for HELP, " +
            "answer y or n to questions,\n and you will get your level of your knowledge";

    @Override
    public void doAction(String userInput) {
        System.out.println(HELP_MESSAGE);
    }
}
