package com.luxoft.dnepr.courses.unit2.model;

public class Hexagon extends Figure {

    private final double side;

    public Hexagon(double side) {
        checkArgument(side);
        this.side = side;
    }

    @Override
    public double calculateArea() {
        return Math.pow(side, 2) * 3 * Math.pow(3, 0.5) / 2;
    }
}
