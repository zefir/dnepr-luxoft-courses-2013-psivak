package com.luxoft.dnepr.courses.regular.unit6.familytree;

import java.io.OutputStream;
import java.io.Serializable;
import java.util.Scanner;

public interface Person extends Serializable {
	
	String getName();
	String getEthnicity();
	Person getFather();
	Person getMother();
	Gender getGender();
	int getAge();
    public void setName(String name);

    public void setEthnicity(String ethnicity);

    public void setFather(Person father);

    public void setMother(Person mother);

    public void setGender(Gender gender);

    public void setAge(int age);

    void save(OutputStream os);
    void read(Scanner scanner);

    void setProperty(String propertyName, String propertyValue);
}
