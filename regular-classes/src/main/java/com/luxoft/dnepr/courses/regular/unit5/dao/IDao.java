package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

public interface IDao<T extends Entity> {
    T save(T e) throws UserAlreadyExist;

    T update(T e) throws UserNotFound;

    T get(long id);

    boolean delete(long id);
}
