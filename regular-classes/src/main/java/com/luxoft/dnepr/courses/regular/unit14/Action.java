package com.luxoft.dnepr.courses.regular.unit14;

public interface Action {

    void doAction(String userInput);

}
