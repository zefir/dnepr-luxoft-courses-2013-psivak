package com.luxoft.dnepr.courses.regular.unit5.model;

public class Redis extends Entity {
    private int weight;

    public Redis() {

    }

    public Redis(Long id) {
        super(id);
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
