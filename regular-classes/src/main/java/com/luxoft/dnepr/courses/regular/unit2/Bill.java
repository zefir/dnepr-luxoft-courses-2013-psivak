package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {

    List<Product> products = new ArrayList<>();

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    public void append(Product product) {
        for (Product temp : products) {
            if (temp.equals(product)) {
                groupProduct(temp, product);
                return;
            }
        }
        products.add(product);
    }

    private void groupProduct(Product localProduct, Product newProduct) {

        if (localProduct.getClass() == newProduct.getClass()) {
            products.add(new CompositeProduct(localProduct, newProduct));
            products.remove(localProduct);
        } else {
            CompositeProduct temp = (CompositeProduct) localProduct;
            temp.add(newProduct);
        }
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return
     */
    public double summarize() {
        double sum = 0;
        for (Product product : products) {
            sum += product.getPrice();
        }
        return sum;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */
    public List<Product> getProducts() throws CloneNotSupportedException {
        List<Product> sortedProducts = new ArrayList<>();
        for (Product product : products) {
            if (product.getClass() == CompositeProduct.class) {
                AbstractProduct temp = (AbstractProduct) ((CompositeProduct) product).getFirst();
                temp = (AbstractProduct) temp.clone();
                temp.setPrice(product.getPrice());
                sortedProducts.add(temp);
            } else {
                sortedProducts.add(product);
            }
        }
        Collections.sort(sortedProducts, new Comparator<Product>() {
            @Override
            public int compare(Product o1, Product o2) {
                return Double.compare(o2.getPrice(), o1.getPrice());
            }
        });
        return sortedProducts;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        try {
            for (Product product : getProducts()) {
                productInfos.add(product.toString());
            }
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }

}
