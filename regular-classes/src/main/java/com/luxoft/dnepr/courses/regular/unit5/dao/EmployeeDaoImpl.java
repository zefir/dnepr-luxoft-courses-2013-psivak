package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

public class EmployeeDaoImpl extends GenericDao<Employee> {

    public EmployeeDaoImpl() {
        super(Employee.class);
    }

}
