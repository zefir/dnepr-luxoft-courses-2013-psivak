package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Date;

public class Book extends AbstractProduct{

    private Date publicationDate;

    public Book(String code, String name, double price, Date publicationDate) {

        super(code, name, price);
        this.publicationDate = publicationDate;

    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public Date getPublicationDate() {

        return publicationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (code != null ? !code.equals(book.code) : book.code != null) return false;
        if (name != null ? !name.equals(book.name) : book.name != null) return false;
        if (publicationDate != null ? !publicationDate.equals(book.publicationDate) : book.publicationDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (publicationDate != null ? publicationDate.hashCode() : 0);
        return result;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Book cloned = (Book) super.clone();
        cloned.publicationDate = (Date) publicationDate.clone();
        return cloned;
    }
}
