package com.luxoft.dnepr.courses.regular.unit5;

import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.dao.GenericDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.*;

import static org.junit.Assert.assertEquals;

public class GenericDaoTest {

    private static String JDBC_URL = "jdbc:h2:test;IFEXISTS=TRUE";
    private static String JDBC_LOGIN = "root";
    private static String JDBC_PASSWORD = "root";
    GenericDao<Redis> redisGenericDao = new RedisDaoImpl();
    GenericDao<Employee> employeeGenericDao = new EmployeeDaoImpl();
    private Connection dbConnection;


    @Before
    public void init() {
        this.dbConnection = getConnection();
    }

    @After
    public void close() throws SQLException {
        dbConnection.close();
    }

    @Test
    public void employeeTest() throws UserAlreadyExist, SQLException, UserNotFound {
        Employee emp1 = new Employee();
        emp1.setSalary(1000);
        Employee emp2 = new Employee();
        emp1.setSalary(2000);

        employeeGenericDao.save(emp1);
        employeeGenericDao.save(emp2);

        String selectNumberOfRows = "SELECT COUNT(salary) as count from EMPLOYEE";
        Statement statement = dbConnection.createStatement();
        ResultSet rs = statement.executeQuery(selectNumberOfRows);
        rs.next();
        assertEquals(2, rs.getInt("count"));
        rs.close();
        emp1.setId(Long.valueOf(1));
        emp1.setSalary(10000);
        employeeGenericDao.update(emp1);
        emp1 = employeeGenericDao.get(1);
        assertEquals(10000, emp1.getSalary());
        employeeGenericDao.delete(1);
        rs = statement.executeQuery(selectNumberOfRows);
        rs.next();
        assertEquals(1, rs.getInt("count"));
        rs.close();
    }

    @Test
    public void redisTest() throws UserAlreadyExist, SQLException, UserNotFound {

        Redis redis1 = redisGenericDao.create();
        Redis redis2 = redisGenericDao.create();
        redis1.setWeight(100);
        redis2.setWeight(200);
        redisGenericDao.save(redis1);
        redisGenericDao.save(redis2);
        String selectNumberOfRows = "SELECT COUNT(weight) as count from Redis";
        Statement statement = dbConnection.createStatement();
        ResultSet rs = statement.executeQuery(selectNumberOfRows);
        rs.next();
        assertEquals(2, rs.getInt("count"));
        rs.close();
        redis1.setId(Long.valueOf(1));
        redis1.setWeight(2000);
        redisGenericDao.update(redis1);
        redis1 = redisGenericDao.get(1);
        assertEquals(2000, redis1.getWeight());
    }

    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(JDBC_URL, JDBC_LOGIN, JDBC_PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

}
