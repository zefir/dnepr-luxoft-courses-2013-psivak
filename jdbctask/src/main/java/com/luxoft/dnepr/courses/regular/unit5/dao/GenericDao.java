package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;


public abstract class GenericDao<T extends Entity> implements IDao<T> {


    private Class<T> type;
    private static String JDBC_URL = "jdbc:h2:test;IFEXISTS=TRUE";
    private static String JDBC_LOGIN = "root";
    private static String JDBC_PASSWORD = "root";

    public GenericDao(Class<T> type) {
        this.type = type;
    }

    @Override
    public T save(T entity) throws UserAlreadyExist {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String tableName = type.getSimpleName();
        Field[] fields = type.getDeclaredFields();
        String columnName = fields[0].getName();
        String insertTableSQL = "INSERT INTO " + tableName + "(" + columnName + ") VALUES(?)";
        try {
            conn = getConnection();
            preparedStatement = conn.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1, getRowValue(entity));
            preparedStatement.execute();
            conn.close();
            return type.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return entity;
    }

    private int getRowValue(T entity) {
        int rowValue = 0;
        try {
            rowValue = (int) type.getDeclaredMethods()[1].invoke(entity);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return rowValue;
    }

    @Override
    public T update(T entity) throws UserNotFound {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String tableName = type.getSimpleName();
        Field[] fields = type.getDeclaredFields();
        String columnName = fields[0].getName();
        String insertTableSQL = "UPDATE " + tableName + " SET " + columnName + "=? " + "where id=" + entity.getId();
        try {
            conn = getConnection();
            preparedStatement = conn.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1, getRowValue(entity));
            preparedStatement.execute();
            conn.close();
            return type.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return entity;
    }

    @Override
    public T get(long id) {
        Connection conn = null;
        Statement stmt = null;
        String tableName = type.getSimpleName();
        Field[] fields = type.getDeclaredFields();
        String columnName = fields[0].getName();
        System.out.println(tableName + " " + columnName);
        try {
            conn = getConnection();
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT " + columnName + " FROM " + tableName + " WHERE id=" +id);
            rs.next();
            T entity = type.newInstance();
            entity.setId(id);
            try {
                type.getDeclaredMethods()[0].invoke(entity, rs.getInt(1));
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            conn.close();

            return entity;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public boolean delete(long id) {
        Connection conn = null;
        Statement statement = null;
        String deleteTableSQL = "DELETE EMPLOYEE WHERE id=" + id;
        try {
            conn = getConnection();
            statement = conn.createStatement();
            statement.executeUpdate(deleteTableSQL);
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    public T create() {
        try {
            return type.getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            return null;
        }
    }

    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(JDBC_URL, JDBC_LOGIN, JDBC_PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
