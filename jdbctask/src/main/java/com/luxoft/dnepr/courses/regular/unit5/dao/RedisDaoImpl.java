package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.model.Redis;

public class RedisDaoImpl extends GenericDao<Redis> {

    public RedisDaoImpl() {
        super(Redis.class);
    }

}
