package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Compiler {

    public static final boolean DEBUG = true;

    public static void main(String[] args) {
        byte[] byteCode = compile(getInputString(args));
        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }

    static byte[] compile(String input) {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        input = input.replaceAll(" +", "");

        parse(input, 0, input.length(), result, (byte) 0);
        addCommand(result, VirtualMachine.PRINT);
        return result.toByteArray();
    }

    private static void parse(String input, int from, int to, ByteArrayOutputStream result, byte operation) {
        if (from >= to) {
            if (operation == VirtualMachine.DIV || operation == VirtualMachine.SUB) {
                addCommand(result, VirtualMachine.SWAP);
            }
            if (operation != 0) {
                addCommand(result, operation);
            }
            return;
        }
        if (input.charAt(from) == '(') {
            if (isOpenBracketNext(input, from + 1)) {
                parse(input, from + 1, input.lastIndexOf(')', to - 1), result, (byte) 0);
            } else {
                parse(input, from + 1, input.indexOf(')', from), result, (byte) 0);
            }
            if (from >= to) {
                return;
            } else {
                if (isOpenBracketNext(input, from + 1)) {
                    parse(input, input.lastIndexOf(')', to - 1) + 1, to, result, operation);
                } else {
                    parse(input, input.indexOf(')', from) + 1, to, result, operation);
                }
            }
            return;
        }
        if (input.charAt(from) == '+' || input.charAt(from) == '-' || input.charAt(from) == '*' || input.charAt(from) == '/') {
            parse(input, from + 1, to, result, getOperation(input.charAt(from)));
        } else {
            int pos = from;
            while (Character.isDigit(input.charAt(pos)) || input.charAt(pos) == '.') {
                pos++;
                if (pos == to) break;
            }
            addCommand(result, VirtualMachine.PUSH);
            writeDouble(result, Double.parseDouble(input.substring(from, pos)));
            parse(input, pos, to, result, operation);
        }

    }

    private static boolean isOpenBracketNext(String input, int i) {
        return !(input.indexOf('(', i) < 0 || input.indexOf('(', i) > input.indexOf(')', i));
    }

    private static byte getOperation(char operation) {

        switch (operation) {
            case '+':
                return VirtualMachine.ADD;
            case '-':
                return VirtualMachine.SUB;
            case '*':
                return VirtualMachine.MUL;
            case '/':
                return VirtualMachine.DIV;

        }
        return 0;
    }

    /**
     * Adds specific command to the byte stream.
     *
     * @param result
     * @param command
     */
    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     *
     * @param result
     * @param command
     * @param value
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));
    }

    private static String getInputString(String[] args) {

        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<>();
        while (scanner.hasNext()) {
            data.add(scanner.next());
        }
        return join(data);
    }

    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }

}
