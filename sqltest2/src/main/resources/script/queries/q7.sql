SELECT DISTINCT maker_name
FROM makers
WHERE maker_id IN (
  SELECT maker_id
  FROM product p JOIN pc ON pc.model=p.model
  WHERE p.type='PC' AND pc.speed >= 450
)
