SELECT DISTINCT t1.maker_name
FROM makers AS t1 JOIN
  (SELECT p.maker_id from product AS p JOIN printer AS pr
   ON p.model=pr.model) AS t2
ON t1.maker_id=t2.maker_id
ORDER BY t1.maker_name DESC
