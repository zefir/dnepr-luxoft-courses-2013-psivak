SELECT avg(hd) AS avg_hd
FROM pc JOIN product p ON p.model = pc.model
WHERE maker_id IN (SELECT maker_id FROM product WHERE type = 'printer')
