SELECT maker_id, avg(hd) AS avg_hd
FROM product, pc
WHERE pc.model = product.model
      AND maker_id IN (SELECT maker_id FROM product WHERE type = 'printer')
GROUP BY maker_id
