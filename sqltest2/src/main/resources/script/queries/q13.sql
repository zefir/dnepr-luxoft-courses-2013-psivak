SELECT DISTINCT type, laptop.model, speed
FROM laptop, product
WHERE laptop.model = product.model AND speed < (SELECT min(speed) FROM pc)
