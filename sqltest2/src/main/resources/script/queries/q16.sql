SELECT maker_id, count(model) AS mod_count
FROM product
WHERE type = 'pc'
GROUP BY maker_id
HAVING count(model) >= 3
