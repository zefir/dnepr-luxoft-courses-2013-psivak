SELECT DISTINCT maker_name, price
FROM product, printer, makers
WHERE makers.maker_id=product.maker_id
      AND product.model = printer.model
      AND printer.color = 'y'
      AND price IN (SELECT min(price) FROM printer WHERE color = 'y')