SELECT avg(screen) AS avg_size, maker_id
FROM product, laptop
WHERE product.model = laptop.model
GROUP BY maker_id
ORDER BY avg_size ASC
