SELECT DISTINCT maker_id,type
FROM product
WHERE maker_id IN (
  SELECT maker_id
  FROM Product
  GROUP BY maker_id
  HAVING max(type) = min(type)
)
AND maker_id IN (
  SELECT maker_id
  FROM product
  GROUP BY maker_id
  HAVING count(model) > 1)
