SELECT DISTINCT t.model
FROM (SELECT model,price FROM laptop WHERE price = (SELECT max(price) FROM laptop)
      UNION
      SELECT model,price FROM pc WHERE price = (SELECT max(price) FROM pc)
      UNION
      SELECT model,price FROM printer WHERE price = (SELECT max(price) FROM printer)
     ) AS t
WHERE t.price >=ALL (SELECT price FROM laptop WHERE price = (SELECT max(price) FROM laptop)
                     UNION
                     SELECT price FROM pc WHERE price = (SELECT max(price) FROM pc)
                     UNION
                     SELECT price FROM printer WHERE price = (SELECT max(price) FROM printer))
