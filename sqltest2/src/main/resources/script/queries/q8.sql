SELECT model, price
FROM printer
WHERE price IN (SELECT max(price) FROM printer)
