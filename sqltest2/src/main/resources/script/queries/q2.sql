SELECT DISTINCT t1.maker_name, t2.speed
FROM makers AS t1 JOIN
  (SELECT p.maker_id, l.speed FROM product AS p JOIN laptop AS l
   ON p.model=l.model AND l.hd >= 10) AS t2
ON t1.maker_id=t2.maker_id

