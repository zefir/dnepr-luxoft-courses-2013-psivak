SELECT model, price
FROM laptop
WHERE model IN (SELECT model FROM (SELECT model FROM product p1 JOIN makers m1 ON p1.maker_id=m1.maker_id WHERE m1.maker_name='B') AS l)
UNION
SELECT model, price
FROM pc
WHERE model IN (SELECT model FROM (SELECT model FROM product p2 JOIN makers m2 ON p2.maker_id=m2.maker_id WHERE m2.maker_name='B') AS p)
UNION
SELECT model, price
FROM printer
WHERE model IN (SELECT model FROM (SELECT model FROM product p3 JOIN makers m3 ON p3.maker_id=m3.maker_id WHERE m3.maker_name='B') AS pr)
