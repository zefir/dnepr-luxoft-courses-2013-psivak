SELECT DISTINCT maker_id
FROM product
WHERE type = 'printer'
      AND maker_id IN (SELECT maker_id FROM product WHERE type = 'PC'
      AND model IN (SELECT model FROM pc where ram = (SELECT min(ram) FROM pc) AND speed = (SELECT max(speed) FROM pc WHERE ram = (SELECT min(ram) FROM pc))) )
