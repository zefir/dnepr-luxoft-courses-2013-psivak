<html>
<head>
    <title>WelcomePage</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<style>
    .well{
        position: absolute;
        top: 0;
        right: 0;
    }
</style>
<body>
<%@ page session="false" %>
    <div class="well well-lg"><a href="logout"><strong>Logout</strong></a></div>
    <h1 class="text-center text-success">
    <%
        out.println("Hello, " + request.getAttribute("login") + "!");
    %>
    </h1>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>