<!DOCTYPE html>
<html>
<head>
    <title>Authorization form</title>

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<style type="text/css">
.form-horizontal{
	margin-top: 250px;
	margin-left: 300px;

}
</style>
<body>
<%@ page session="false" %>
<div class="container">
	
	<form method="POST" action="auth" class="form-horizontal col-sm-8" role="form">
        <div class="alert alert-danger <% if (request.getAttribute("auth") == null) {out.print("hidden");} %>">Wrong login or password!
            <a id="my_alert" class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
        </div>
  		<div class="form-group">
    		<label for="login" class="col-sm-2 control-label">Login</label>
    		<div class="col-sm-4">
      			<input name="login" type="text" class="form-control" id="login" placeholder="Login">
    		</div>
  		</div>
  		<div class="form-group">
    		<label for="password" class="col-sm-2 control-label">Password</label>
    		<div class="col-sm-4">
      			<input name="password" type="password" class="form-control" id="password" placeholder="Password">
    		</div>
  		</div>
  		<div class="form-group">
    		<div class="col-sm-offset-2 col-sm-10">
      			<div class="checkbox">
        			<label>
          				<input type="checkbox"> Remember me
        			</label>
      			</div>
    		</div>
  		</div>
  		<div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
      			<button type="submit" class="btn btn-success">Sign in</button>
    		</div>
  		</div>
	</form>
	
</div>
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>
</html>