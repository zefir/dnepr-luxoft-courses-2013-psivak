package com.luxoft.dnepr.courses.regular.unit11;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

public class WelcomePageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if (session != null) {
            Cookie[] cookies = req.getCookies();
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("login")) {
                    req.setAttribute("login", cookie.getValue());
                }
            }
            if (req.getAttribute("login") == null) {
                req.setAttribute("login", "Cookie clean man :)");
            }
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("welcome.jsp");
            requestDispatcher.forward(req, resp);
        } else {
            resp.sendRedirect("");
            return;
        }
    }
}
