package com.luxoft.dnepr.courses.regular.unit11;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ParseXmlListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext ctx = servletContextEvent.getServletContext();
        Map<String, String> userDB = new HashMap<>();
        String path = ctx.getRealPath("/") + "META-INF/" + ctx.getInitParameter("users");
        //ctx.log(path);
        File file = new File(path);

        //ctx.log(file.getAbsolutePath());
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document doc = null;

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            doc = builder.parse(file);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Element root = doc.getDocumentElement();
        NodeList children = root.getChildNodes();
        root.getElementsByTagName("user");
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof Element) {
                userDB.put(((Element) child).getAttribute("name"),((Element) child).getAttribute("password"));
            }
        }
        ctx.setAttribute("userDB", userDB);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
