package com.luxoft.dnepr.courses.regular.unit11;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class DummyServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=utf-8");
        PrintWriter writer = resp.getWriter();
        String reqParamName = req.getParameter("name");
        String reqParamAge = req.getParameter("age");
        if (reqParamName == null || reqParamAge == null) {
            resp.setStatus(500);
            writer.print("{\"error\": \"Illegal parameters\"}");
        } else {
            ServletContext ctx = getServletContext();
            if (ctx.getAttribute(reqParamName) != null) {
                resp.setStatus(500);
                writer.print("{\"error\": \"Name " + reqParamName + " already exists\"}");
            } else {
                synchronized (ctx) {
                    ctx.setAttribute(reqParamName, reqParamAge);
                }
                resp.setStatus(201);
            }
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=utf-8");
        PrintWriter writer = resp.getWriter();
        String reqParamName = req.getParameter("name");
        String reqParamAge = req.getParameter("age");
        if (reqParamName == null || reqParamAge == null) {
            resp.setStatus(500);
            writer.print("{\"error\": \"Illegal parameters\"}");
        } else {
            ServletContext ctx = getServletContext();
            if (ctx.getAttribute(reqParamName) == null) {
                resp.setStatus(500);
                writer.print("{\"error\": \"Name " + reqParamName + " does not exist\"}");
            } else {
                synchronized (ctx) {
                    ctx.setAttribute(reqParamName, reqParamAge);
                }
                resp.setStatus(202);
                writer.print("{\"" + reqParamName + "\":\"" + reqParamAge + "\"}");
            }
        }
    }
}

