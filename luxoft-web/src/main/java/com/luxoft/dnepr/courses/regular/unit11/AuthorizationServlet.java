package com.luxoft.dnepr.courses.regular.unit11;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AuthorizationServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ServletContext ctx = getServletContext();
        HashMap<String, String> userDB = (HashMap<String, String>) ctx.getAttribute("userDB");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        boolean isAuth = false;

        for (Map.Entry<String, String> entry : userDB.entrySet()) {
            if (entry.getKey().equals(login) && entry.getValue().equals(password)) {
                isAuth = true;
                req.getSession();
                Cookie cookie = new Cookie("login", login);
                cookie.setMaxAge(180);
                resp.addCookie(cookie);
                resp.sendRedirect("user");
            }
        }
        if (!isAuth) {
            req.setAttribute("auth", 0);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("");
            requestDispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getRequestURI().contains("logout")) {
            req.getSession().invalidate();
            resp.sendRedirect("");
            return;
        }
    }
}
