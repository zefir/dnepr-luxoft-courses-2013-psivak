CREATE TABLE IF NOT EXISTS makers (
    maker_id INT AUTO_INCREMENT ,
    maker_name VARCHAR(50) NOT NULL,
    maker_adress VARCHAR(200),
    CONSTRAINT pk_makers PRIMARY KEY(maker_id)
);
INSERT INTO makers(maker_name, maker_adress)
  VALUES('A', 'AdressA'),('B', 'AdressB'),('C', 'AdressC'),('D', 'AdressD'),('E', 'AdressE');

ALTER TABLE product
  ADD maker_id INT NOT NULL ;

UPDATE product p ,makers m
  SET p.maker_id=m.maker_id
  WHERE p.maker=m.maker_name;

ALTER TABLE product
  DROP COLUMN maker,
  ADD CONSTRAINT fk_product_makers FOREIGN KEY(maker_id) REFERENCES makers(maker_id);

CREATE TABLE IF NOT EXISTS printer_type (
	type_id INT,
	type_name VARCHAR(50) NOT NULL,
	CONSTRAINT pk_printer_type PRIMARY KEY(type_id)
);

INSERT INTO printer_type(type_id, type_name)
  VALUES(1, 'Laser'), (2, 'Jet'), (3, 'Matrix');

ALTER TABLE printer
  ADD COLUMN type_id INT NOT NULL;

UPDATE printer p ,printer_type pt
  SET p.type_id=pt.type_id
  WHERE p.type=pt.type_name;

ALTER TABLE printer
  ADD CONSTRAINT fk_printer_type FOREIGN KEY(type_id) REFERENCES printer_type(type_id);

ALTER TABLE printer
  DROP COLUMN type,
  CHANGE color color char(1) NOT NULL;

ALTER TABLE printer
  ALTER COLUMN color SET DEFAULT 'y';

CREATE INDEX ind_pc_price ON pc (price ASC) USING BTREE;

CREATE INDEX ind_laptop_price ON laptop (price ASC) USING BTREE;

CREATE INDEX ind_printer_price ON printer (price ASC) USING BTREE;

  